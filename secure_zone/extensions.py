'''
This file is part of FrontLine.

FrontLine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FrontLine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FrontLine.  If not, see <https://www.gnu.org/licenses/>.
'''

import persistent
import persistent.list
from secure_zone.protected_extension import ProtectedExtension
from agents.win import filesystem

class Extensions(persistent.list.PersistentList):
	def __init__(self, protected_extensions, excluded_extensions, files, path):
		self.init_protectedExtensions(protected_extensions)
		self.init_excludedExtensions(excluded_extensions)
		self.init_protectedFiles(files, path)

	def init_protectedExtensions(self, protected_extensions):
		self.protectedExtensions = persistent.list.PersistentList()
		for protected_extension in protected_extensions:
			self.protect(protected_extension, protected_extensions[protected_extension])

	def protect(self, protected_extension, protected_extension_data):
		protectedExtension = ProtectedExtension(protected_extension, protected_extension_data)
		self.protectedExtensions.append(protectedExtension)
		self._p_changed = 1

	def protected(self, extension):
		for protectedExtension in self.protectedExtensions:
			if protectedExtension.extension.upper() == extension.upper():
				return True
		return False

	def listProtected(self):
		return self.protectedExtensions

	def get(self, extension):
		for protectedExtension in self.protectedExtensions:
			if protectedExtension.extension.upper() == extension.upper():
				return protectedExtension
		return None

	def add(self, extension, file, path):
		for protectedExtension in self.protectedExtensions:
			if protectedExtension.extension.upper() == extension.upper():
				protectedExtension.add(file, path)
				self._p_changed = 1

	def init_excludedExtensions(self, excluded_extensions):
		self.excludedExtensions = persistent.list.PersistentList()
		for excluded_extension in excluded_extensions:
			self.exclude(excluded_extension)

	def exclude(self, excluded_extension):
		self.excludedExtensions.append(excluded_extension)
		self._p_changed = 1

	def excluded(self, extension):
		return extension in self.excludedExtensions

	def allowed(self, extension):
		return self.protected(extension) and not self.excluded(extension)

	def init_protectedFiles(self, files, path):
		for file in files:
			self.checkFileAndAdd(file, path)
		self._p_changed = 1

	def checkFileAndAdd(self, file, path):
		extension = filesystem.extension(file.name)
		if self.allowed(extension):
			self.add(extension, file, path)
			self._p_changed = 1

	def __str__(self):
		text = "------------ Protected Extensions: \n"
		for protectedExtension in self.protectedExtensions:
			text += str(protectedExtension)
		text += "------------ Excluded Extensions: \n"
		for excludedExtension in self.excludedExtensions:
			text += str(excludedExtension) + "\n"
		return text

	def fileList(self, namingRules = False):
		list = []
		for protectedExtension in self.protectedExtensions:
			list += protectedExtension.fileList(namingRules)
		return list

	def fileSize(self):
		size = 0
		for protectedExtension in self.protectedExtensions:
			size += protectedExtension.fileSize()
		return size

	def addHoneyFile(self, subpathList, honeyFile):
		if len(subpathList) == 1:
			filename = subpathList[0]
			extension = filesystem.extension(filename)
			protectedExtension = self.get(extension)
			if protectedExtension:
				protectedExtension.addHoneyFile(filename, honeyFile)

	def newEventReceived(self, subpathList, event):
		if len(subpathList) == 1:
			filename = subpathList[0]
			extension = filesystem.extension(filename)
			protectedExtension = self.get(extension)
			if protectedExtension:
				protectedExtension.newEventReceived(filename, event)
