'''
This file is part of FrontLine.

FrontLine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FrontLine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FrontLine.  If not, see <https://www.gnu.org/licenses/>.
'''

import persistent
import persistent.list
from pprint import pprint
import os, time, json, os.path, copy
from pathlib import Path
from pprint import pprint
from secure_zone.secure_zone import SecureZone
from agents.win import filesystem

class SecureZones(persistent.Persistent):
	SECURE_ZONE_FILE = '\\storage\\secure_zones.json'

	def __init__(self, root, driveMapper):
		self.process_secureZones(root, driveMapper)
		#print("******************* FileList ***************************")
		#pprint(self.fileList())
		#print("******************* -------- ***************************")

	def process_secureZones(self, root, driveMapper):
		self.secureZones = persistent.list.PersistentList()
		self.SECURE_ZONE_FILE = root + self.SECURE_ZONE_FILE
		with open(self.SECURE_ZONE_FILE, 'r') as f:
			zones = json.load(f)
		for path in zones:
			path_sys = driveMapper.rmap(path)
			secureZone = SecureZone(path, path_sys, zones[path])
			self.add(secureZone)

	def add(self, secureZone):
		self.secureZones.append(secureZone)
		self._p_changed = 1

	def zones(self):
		return self.secureZones

	def get(self, fullpath):
		for secureZone in self.secureZones:
			if secureZone.path() == fullpath:
				return True, secureZone
		return False, None

	def getFileZone(self, file):
		length = 0
		zone = None
		filepath = filesystem.dirname(file).upper()
		for secureZone in self.secureZones:
			if filepath.startswith(secureZone.path.upper()):
				zone = secureZone if len(secureZone.path) > length else zone
		return zone

	def __str__(self):
		text = "////////// SecureZones ////////////////" + "\n"
		for secureZone in self.secureZones:
			text += str(secureZone)
		return text

	def fileList(self, namingRules = False):
		list = []
		for secureZone in self.secureZones:
			list += secureZone.fileList(namingRules)
		return list

	def fileSize(self):
		size = 0
		for secureZone in self.secureZones:
			size += secureZone.fileSize()
		return size

	def addHoneyFile(self, protectedFile, honeyFile):
		zone = self.getFileZone(protectedFile)
		if zone:
			zone.addHoneyFile(protectedFile, honeyFile)

	def newEventReceived(self, protectedFile, event):
		zone = self.getFileZone(protectedFile)
		if zone:
			zone.newEventReceived(protectedFile, event)
