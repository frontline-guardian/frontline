'''
This file is part of FrontLine.

FrontLine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FrontLine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FrontLine.  If not, see <https://www.gnu.org/licenses/>.
'''

import persistent
import persistent.list

import json

from agents.win import filesystem
from honey_zone.honey_zone import HoneyZone
from utils.color import Color

class HoneyZones(persistent.Persistent):
	DB_ENTRY = 'honeyZones'

	SECURE_ZONE_FILE = '\\storage\\secure_zones.json'

	def __init__(self, root, driveMapper):
		self.process_honeyZones(root, driveMapper)

	def process_honeyZones(self, root, driveMapper):
		self.honeyZones = persistent.list.PersistentList()
		self.SECURE_ZONE_FILE = root + self.SECURE_ZONE_FILE
		with open(self.SECURE_ZONE_FILE, 'r') as f:
			zones = json.load(f)
		for path in zones:
			path_sys = driveMapper.rmap(path)
			honeyZone = HoneyZone(path, path_sys, zones[path])
			self.add(honeyZone)
		for drive in driveMapper.rlist().keys():
			print(drive)
			drive += "\\"
			path_sys = driveMapper.rmap(path)
			honeyZone = HoneyZone(drive, path_sys, {}, False)
			#self.add(honeyZone)

	def add(self, honeyZone):
		self.honeyZones.append(honeyZone)
		self._p_changed = 1

	def zones(self):
		return self.honeyZones

	def dirList(self):
		list = []
		for zone in self.honeyZones:
			list += zone.dirList()
		return list

	def get(self, fullpath):
		for honeyZone in self.honeyZones:
			if honeyZone.path() == fullpath:
				return honeyZone
		return None

	def getFileZone(self, file):
		length = 0
		zone = None
		filepath = filesystem.dirname(file).upper()
		for honeyZone in self.honeyZones:
			if filepath.startswith(honeyZone.path.upper()):
				#print(Color.purple("{} zone is {}".format(filepath, honeyZone.path)))
				#print(Color.blue(""))
				zone = honeyZone if len(honeyZone.path) > length else zone
		return zone

	def __str__(self):
		text = "////////// HoneyZones ////////////////" + "\n"
		for honeyZone in self.honeyZones:
			text += str(honeyZone)
		return text

	def fileList(self):
		list = []
		for honeyZone in self.honeyZones:
			list += honeyZone.fileList()
		return list

	def fileSize(self):
		size = 0
		for honeyZone in self.honeyZones:
			size += honeyZone.fileSize()
		return size

	def addHoneyFile(self, protectedFile, protectedFile_extension_stats, honeyFile):
		try:
			zone = self.getFileZone(honeyFile)
			if zone:
				return zone.addHoneyFile(protectedFile, protectedFile_extension_stats, honeyFile)
			print("{} zone doesn't exist".format(honeyFile))
		except Exception as e:
			print("ERROR honey_zones addHoneyFile#0001: {}".format(e))
		finally:
			return None


	def whatChanged(self, honeyFile):
		zone = self.whatChanged(honeyFile)
		if zone:
			zone.whatChanged(honeyFile)
