'''
This file is part of FrontLine.

FrontLine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FrontLine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FrontLine.  If not, see <https://www.gnu.org/licenses/>.
'''

import persistent, persistent.list

import time

from monitor.classification import Classification
from utils.color import Color
from gui_manager import GUIManager

class FilesystemEvent(persistent.Persistent):
	PREFIX	  =   '{-|FL|'
	READ		=   'RD|'
	WRITE	   =   'WR|'
	SEPARATOR   =   '|-'
	SUFFIX	  =   '|-}'
	HEADER_SEP  =   '|'

	def __init__(self, event, driveMapper):
		try:
			filesystem_id, filesystem_timestamp, filesystem_event_message = event.split("\t")
			prefix_parsed_items = filesystem_event_message.split(self.PREFIX)
			if len(prefix_parsed_items) > 1:
				self.filesystem_id  = filesystem_id
				self.filesystem_timestamp = filesystem_timestamp
				self.timestamp = int(round(time.time() * 1000))
				suffix_parsed_items = prefix_parsed_items[1].split(self.SUFFIX)
				header, self.buffer = suffix_parsed_items[0].split(self.SEPARATOR)
				self.operation, self.process, self.process_id, self.file_fullpath = header.split(self.HEADER_SEP)
				self.file_fullpath = driveMapper.map(self.file_fullpath)
				self.process = driveMapper.map(self.process)
				self.process_id = int(self.process_id)
				self.classifications = persistent.list.PersistentList()
				self.valid = True
				self._p_changed = 1
			else:
				#print("ERROR FilesystemEvent-Init#002: {}".format(e))
				self.valid = False
		except Exception as e:
			self.valid = False
			GUIManager.log("ERROR FilesystemEvent-Init#001: event = {}".format(event))
		finally:
			pass

	def addClassification(self, source, result):
		self.classifications.append(Classification(source, result, self.timestamp))
		self._p_changed = 1

	def ransomware(self, source):
		self.addClassification(source, 'ransomware')
		GUIManager.addRansomwareEvent(self)
		GUIManager.addattackedFile(self)
		#print(Color.bold(Color.red("ALERT: A POSSIBLE RANSOMWARE ATTACK WAS DETECTED, LOCKING DOWN THE SYSTEM \n")))
		#print("{}\n---------------------------------".format(str(self)))

	def benign(self, source):
		self.addClassification(source, 'benign')

	def ignored(self, source):
		self.addClassification(source, 'ignored')

	def excluded(self, source):
		self.addClassification(source, 'excluded')

	def __str__(self):
		text = ""
		try:
			text +=  'FS-ID: {} - '.format(self.filesystem_id)
			text +=  'FS-Timestamp: {} - '.format(self.filesystem_timestamp)
			text +=  'FL-Timestamp: {} - '.format(self.timestamp)
			text +=  'Process: {}@{}:{} ==> '.format(self.process, self.process_id, self.operation)
			text +=  'file_fullpath: {}\n'.format(self.file_fullpath)
			text +=  'Classifications:\n'
			for classification in self.classifications:
				text +=  '\t{}\n'.format(str(classification))
		except Exception as e:
			pass
		return text
