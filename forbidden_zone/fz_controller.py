'''
This file is part of FrontLine.

FrontLine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FrontLine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FrontLine.  If not, see <https://www.gnu.org/licenses/>.
'''

import ZODB, ZODB.FileStorage, transaction
import persistent
import persistent.list

import queue, threading, time

from forbidden_zone.fz_anomaly_detector import FZ_AnomalyDetector
from forbidden_zone.forbidden_zones import ForbiddenZones
from utils.color import Color
from gui_manager import GUIManager

def threaded(fn):
	def wrapper(*args, **kwargs):
		thread = threading.Thread(target=fn, args=args, kwargs=kwargs)
		thread.start()
		return thread
	return wrapper

class FZ_Controller(threading.Thread):
	stop = False
	IDENTIFIER = "FZ_Controller"
	DB_ENTRY = 'forbiddenZones'
	DB_PATH = './storage/db/'
	requestQueue = queue.Queue()
	eventQueue = queue.Queue()

	def __init__(self, driveMapper, terminator):
		threading.Thread.__init__(self)
		self.intitialised = False
		self.anomalyDetector = FZ_AnomalyDetector(terminator)
		self.anomalyDetector.start()
		try:
			self.getDB()
			self.init_db(driveMapper)
			GUIManager.log("£££££££££££££££££££££ FZ_Controller is intitialised £££££££££££££££££££££££££")
			self.intitialised = True
		except Exception as e:
				GUIManager.log("ERROR FZ __init__#0014: {}".format(e))

	def getDB(self):
		try:
			db_path = '{}/{}.fs'.format(self.DB_PATH,self.DB_ENTRY)
			storage = ZODB.FileStorage.FileStorage(db_path)
			c = threading.Condition()
			db = ZODB.DB(storage)
			self.transaction_manager = transaction.TransactionManager()
			connection = db.open(self.transaction_manager)
			self.root = connection.root()
			return True
		except Exception as e:
			GUIManager.log("ERROR FZ getDB#0016: {}".format(e))
			return False

	def init_db(self, driveMapper):
		try:
			if self.exists():
				GUIManager.log("ForbiddenZones Were Found in the DB")
				self.forbiddenZones = self.root[self.DB_ENTRY]
			else:
				GUIManager.log("No ForbiddenZones Were Found in the DB")
				self.root[self.DB_ENTRY] = ForbiddenZones(driveMapper)
				self.forbiddenZones = self.root[self.DB_ENTRY]
				self.transaction_manager.commit()
			self.eventReceiver()
			self.committer()
			return True
		except Exception as e:
			GUIManager.log("ERROR FZ init_db#0017: {}".format(e))
			return False

	def exists(self):
		return 	self.DB_ENTRY in self.root

	def zones(self):
		return self.forbiddenZones.zones()

	def addRequest(self, request):
		self.requestQueue.put(request)

	def addNewEvent(self, event):
		self.eventQueue.put(event)

	def whatChanged(self, forbiddenFile):
		return self.forbiddenZones.whatChanged(forbiddenFile)

	def restore(self, forbiddenFile):
		restored = forbiddenFile.restore()
		return restored

	def getForbiddenFile(self, forbiddenFile_FullPath):
		for item in self.forbiddenFileList:
			if item["fullpath"] == forbiddenFile_FullPath:
				return item
		return None

	def isForbiddenFile(self, file):
		return True if self.forbiddenZones.getFileZone(file) else False

	def terminate(self):
		self.stop = True

	@threaded
	def eventReceiver(self):
		while not self.stop:
			while not self.eventQueue.empty() and not self.stop:
				try:
					event = self.eventQueue.get()
					if self.isForbiddenFile(event.file_fullpath):
						self.anomalyDetector.submit(event)
					else:
						event.ignored(self.IDENTIFIER)
				except KeyboardInterrupt:
					exit()
				except Exception as e:
					GUIManager.log("ERROR FZ eventReceiver#0011: {}".format(e))
			else:
				time.sleep(0.5)

	@threaded
	def committer(self):
		while not self.stop:
			try:
				self.transaction_manager.commit()
			except KeyboardInterrupt:
				exit()
			except Exception as e:
				GUIManager.log("ERROR FZ committer#0015: {}".format(e))
			finally:
				time.sleep(2)
				pass

	def run(self):
		while not self.stop:
			try:
				while not self.requestQueue.empty() and not self.stop:
					request = self.requestQueue.get()
					self.dispatch(request)
			except KeyboardInterrupt:
				exit()
			except Exception as e:
				GUIManager.log("ERROR FZ run#002: {}".format(e))
			finally:
				time.sleep(1)
				pass

	def dispatch(self, request):
		function = request['function']
		data = request['data']
		try:
			if function == "updateForbiddenFile":
				self.forbiddenZones.updateForbiddenFile(data['forbiddenFile'], data['update'])
			elif function == "newEventReceived":
				self.forbiddenZones.newEventReceived(data['forbiddenFile'], data['event'])
			else:
				pass

		except Exception as e:
			GUIManager.log("ERROR dispatch#001: {}".format(e))
