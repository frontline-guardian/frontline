'''
This file is part of FrontLine.

FrontLine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FrontLine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FrontLine.  If not, see <https://www.gnu.org/licenses/>.
'''

import persistent
import persistent.list

from datetime import datetime

from agents.win import filesystem
from statistics import statistics


class HoneyFile(persistent.Persistent):
	def __init__(self,  name, fullpath, protectedFile, extension_stats):
		self.name = name
		self.fullpath = fullpath
		self.path = filesystem.dirname(self.fullpath)
		self.extension = filesystem.extension(self.name)
		self.protectedFile = protectedFile
		self.attributes = filesystem.file_attributes(self.path, self.name)
		self.stats = statistics.stats(self.fullpath, extension_stats) if extension_stats else {}
		self.creationTimestamp = datetime.timestamp(datetime.now())
		self._p_changed = 1

	def __str__(self):
		text  = "----------------------------" + 'FullPath: {}\n'.format(self.fullpath)
		text += "----------------------------" + 'Path: {}\n'.format(self.path)
		text += "----------------------------" + 'Name: {}\n'.format(self.name)
		text += "----------------------------" + 'Extension: {}\n'.format(self.extension)
		text += "----------------------------" + 'ProtectedFile: {}\n'.format(self.protectedFile)
		text += "----------------------------" + 'Attributes: {}\n'.format(self.attributes)
		text += "----------------------------" + 'Stats: {}\n'.format(self.stats)
		#text += 'Parents: {}\n'.format(self.parents)

		return text

	def fileList(self):
		return [self.fullpath]

	def fileSize(self):
		return 0 if not self.attributes else self.attributes['st_size']

	def compare(attributes):
		changes = []
		for key in self.attributes:
			if attributes[key] != self.attributes[key]:
				changes.append({'attribute': key, 'honeyFile': self.attributes[key], 'protectedFile': attributes[key]})
		return changes

	def whatChanged(self):
		attributes = filesystem.file_attributes(self.path, self.name)
		return self.compare(attributes)
