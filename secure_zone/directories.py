'''
This file is part of FrontLine.

FrontLine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FrontLine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FrontLine.  If not, see <https://www.gnu.org/licenses/>.
'''

import persistent
import persistent.list
from secure_zone.extensions import Extensions
from agents.win import filesystem

from statistics import statistics

class Directories(persistent.list.PersistentList):
	def __init__(self,  path, protected_extensions, excluded_extensions, isRoot = False):
		self.isRoot = isRoot
		self.path = path
		self.protected_extensions = protected_extensions
		self.excluded_extensions = excluded_extensions

		if path:
			self.init_protectedDirectories()
		#self.attributes = filesystem.attributes(file.stat())

	def init_protectedDirectories(self):
		self.protectedDirectories = persistent.list.PersistentList()
		if self.isRoot:
			self.addRoot()
		directories = filesystem.get_directories(self.path)
		for directory in directories:
			protectedDirectory = ProtectedDirectory(directory, self.path, self.protected_extensions, self.excluded_extensions)
			self.protectedDirectories.append(protectedDirectory)
			self._p_changed = 1

	def addRoot(self):
		print("Root Directory : {}".format(self.path))
		protectedDirectory = ProtectedDirectory("", self.path, self.protected_extensions, self.excluded_extensions)
		self.protectedDirectories.append(protectedDirectory)
		self._p_changed = 1

	def listProtected(self):
		return self.protectedDirectories

	def get(self, name):
		for protectedDirectory in self.protectedDirectories:
			if protectedDirectory.name.upper() == name.upper():
				return protectedDirectory
		return None
	'''
	def add(self, extension, file, path):
		for protectedDirectory in self.protectedDirectories:
			if protectedDirectory.name.upper() == extension.upper():
				protectedDirectory.add(file, path)
				self._p_changed = 1
	'''
	def fileList(self, namingRules = False):
		if (self.path == ""):
			print("Should be Empty")
			return []
		list = []
		for protectedDirectory in self.protectedDirectories:
			list += protectedDirectory.fileList(namingRules)
		return list

	def fileSize(self):
		size = 0
		for protectedDirectory in self.protectedDirectories:
			size += protectedDirectory.fileSize()
		return size

	def addHoneyFile(self, subpathList, honeyFile):
		if len(subpathList) == 1:
			self.get("").addHoneyFile(subpathList, honeyFile)
		elif len(subpathList) > 1:
			directory = self.get(subpathList[0])
			directory.addHoneyFile(subpathList[1:], honeyFile)
		else:
			pass

	def newEventReceived(self, protectedFile, event):
		if len(subpathList) == 1:
			self.get("").newEventReceived(subpathList, event)
		elif len(subpathList) > 1:
			directory = self.get(subpathList[0])
			directory.newEventReceived(subpathList[1:], event)
		else:
			pass

	def __str__(self):
		text = "------------ Protected Directories: \n"
		for protectedDirectory in self.protectedDirectories:
			text += str(protectedDirectory)
		return text

class ProtectedDirectory(persistent.Persistent):
	def __init__(self,  directory, path, protected_extensions, excluded_extensions):
		self.path = path
		if directory == "":
			self.name = ""
			self.fullpath = self.path
			self.hasSubdirectories = False
			self.directories = []
		else:
			self.name = directory.name
			self.fullpath = filesystem.fullpath(self.path, self.name)
			self.directories = Directories(self.fullpath, protected_extensions, excluded_extensions)
			self.hasSubdirectories = len(filesystem.get_directories(self.fullpath))

		print("Processing SecureZone {}: {}".format(self.name, self.fullpath))
		files = filesystem.get_files(self.fullpath)
		self.extensions = Extensions(protected_extensions, excluded_extensions, files, self.fullpath)

		self.attributes = None if self.name == "" else filesystem.attributes(directory.stat())
		self._p_changed = 1

	def fileList(self, namingRules = False):
		list = []
		list += self.extensions.fileList(namingRules)
		if self.hasSubdirectories :
			list += self.directories.fileList(namingRules)
		return list

	def fileSize(self):
		size = 0
		size += self.extensions.fileSize()
		if self.hasSubdirectories :
			size += self.directories.fileSize()
		return size

	def __str__(self):
		text  = "----------------------------" + 'FullPath: {}\n'.format(self.fullpath)
		text += "----------------------------" + 'Path: {}\n'.format(self.path)
		text += "----------------------------" + 'Name: {}\n'.format(self.name)
		text += "----------------------------" + 'Attributes: {}\n'.format(self.attributes)
		text += "----------------------------{}\n".format(str(self.directories))
		text += "----------------------------:\n" + str(self.extensions)
		#text += 'Parents: {}\n'.format(self.parents)

		return text

	def addHoneyFile(self, subpathList, honeyFile):
		if len(subpathList) == 1:
			self.extensions.addHoneyFile(subpathList, honeyFile)
		elif len(subpathList) > 1 and self.hasSubdirectories:
			self.directories.addHoneyFile(subpathList, honeyFile)
		else:
			pass

	def newEventReceived(self, subpathList, event):
		if len(subpathList) == 1:
			self.extensions.newEventReceived(subpathList, event)
		elif len(subpathList) > 1 and self.hasSubdirectories:
			self.directories.newEventReceived(subpathList, event)
		else:
			pass
