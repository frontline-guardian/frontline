'''
This file is part of FrontLine.

FrontLine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FrontLine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FrontLine.  If not, see <https://www.gnu.org/licenses/>.
'''

import ZODB, ZODB.FileStorage, transaction
import persistent
import persistent.list

from pprint import pprint
import queue, threading, time, os, subprocess, psutil, signal
from utils.unique_queue import OrderedSetPriorityQueue as UQueue

from utils.color import Color
from gui_manager import GUIManager

def threaded(fn):
	def wrapper(*args, **kwargs):
		thread = threading.Thread(target=fn, args=args, kwargs=kwargs)
		thread.start()
		return thread
	return wrapper

class Terminator(threading.Thread):
	greenMile = UQueue()
	excluded = []
	watchlist = []
	generic = ['explorer.exe']
	def __init__(self):
		threading.Thread.__init__(self)
		self.excludedFinder()
		self.terminator()
		self.guardian()

	def excludedFinder(self):
		pid = os.getpid()
		self.exclude(pid, parent = True, children = True, recursive = True)

	def exclude(self, pid, parent = False, children=False, recursive = False):
		try:
			if (pid > -1) and pid not in self.excluded:
				self.excluded.append(pid)
				GUIManager.addExcludedProcess(psutil.Process(pid).name(), pid)

			process = psutil.Process(pid)
			if parent:
				if recursive:
					for parent in process.parents():
						if parent.name() not in self.generic and parent.pid not in self.excluded:
							self.excluded.append(parent.pid)
							GUIManager.addExcludedProcess(psutil.Process(parent.pid).name(), parent.pid)
				elif process.ppid not in self.excluded:
					self.excluded.append(process.ppid)
					GUIManager.addExcludedProcess(psutil.Process(process.ppid).name(), process.ppid)

			if children:
				for child in process.children(recursive=recursive):
					if child.name() not in self.generic and child.pid not in self.excluded:
						self.excluded.append(child.pid)
						GUIManager.addExcludedProcess(psutil.Process(child.pid).name(), child.pid)
		except Exception as e:
			GUIManager.log("ERROR Terminator - Process exclude #004:{} \n ".format(e))

	@threaded
	def terminator(self):
		print(Color.red("Process Terminator Is UP"))
		while True:
			try:
				event = self.greenMile.pop()
				process_name = event.process.split("\\")[-1:][0]
				self.terminate_process(process_name)
			except Exception as e:
				print("ERROR Terminator - Process Terminator #001:{} \n ".format(e))
		GUIManager.log(Color.cyan("Process Terminator Is DOWN"))

	@threaded
	def guardian(self):
		print(Color.purple("\The Guardian is starting his shift\n"))
		while True:
			for process_name in self.watchlist:
				try:
					self.terminate_process(process_name)
				except psutil.NoSuchProcess as e2:
					#print("ERROR Terminator - Process Terminator - NoSuchProcess #007:{} - {}\n ".format(e2, process))
					pass
				except Exception as e:
					GUIManager.log("ERROR Terminator - Guardian #001:{} \n ".format(e))
			time.sleep(0.5)

	@threaded
	def add(self, event, source, silent = False):
		if not event.process_id in self.excluded:
			self.greenMile.insert(event)
			event.ransomware(source)
			process_name = event.process.split("\\")[-1:][0]
			if process_name not in self.watchlist:
				self.watchlist.append(process_name)
				GUIManager.addRansomwareProcess(event.process, process_name, event.process_id)

	def terminate_tree(self, process):
		if process:
			#print(process)
			#parents = [process.parent()]
			#children = process.children(recursive=True)

			self.terminate(process)
			#self.terminate(process.ppid())
			#for child in children:
			#	self.terminate_tree(child)
			#for parent in parents:
			#	self.terminate_tree(parent)


	def terminate(self, process):
		try:
			if not process.pid in self.excluded:
				#process.terminate()
				#print(Color.red("Process {} has been terminated by Terminate Command".format(process.name())))
				pass
		except psutil.AccessDenied as e1:
			GUIManager.log("ERROR Terminator - Process Terminator - AccessDenied #006:{} \n ".format(e1))
			self.kill(process)
		except psutil.NoSuchProcess as e2:
			#print("ERROR Terminator - Process Terminator - NoSuchProcess #007:{} - {}\n ".format(e2, process))
			pass

	def kill(self, process):
		try:
			process.kill()
			print(Color.red("Process {} has been terminated by Kill Command".format(process.name())))
		except psutil.AccessDenied as e1:
			GUIManager.log("ERROR Terminator - Process Terminator - AccessDenied #008:{} \n ".format(e1))
			self.send_signal(process, signal.SIGTERM)
		except psutil.NoSuchProcess as e2:
			#print("ERROR Terminator - Process Terminator - NoSuchProcess #009:{} - {}\n ".format(e2, process))
			pass

	def send_signal(self, process, sig):
		try:
			process.send_signal(sig)
			GUIManager.log(Color.red("Process {} has been terminated by Send_signal Command".format(process.name())))
		except psutil.AccessDenied as e1:
			GUIManager.log("ERROR Terminator - Process Terminator - AccessDenied #010:{} \n ".format(e1))
		except psutil.NoSuchProcess as e2:
			#print("ERROR Terminator - Process Terminator - NoSuchProcess #011:{} - {}\n ".format(e2, process))
			pass

	def terminate_process(self, process_name):
		processes = self.find_process_by_name(process_name)
		#print("{} : {}".format(process_name, processes))
		for process in processes:
			self.terminate_tree(process)

	# Original Copied from https://psutil.readthedocs.io/en/latest/
	def find_process_by_name(self, name):
		list = []
		for p in psutil.process_iter(["name", "exe", "cmdline"]):
			if name == p.info['name'] or \
					p.info['exe'] and os.path.basename(p.info['exe']) == name or \
					p.info['cmdline'] and p.info['cmdline'][0] == name:
				list.append(p)
		return list

	def isWatched(self, filesystemEvent):
		try:
			if (filesystemEvent.process.upper() in self.watchlist) or (filesystemEvent.process_id in self.watchlist):
				return True
		except Exception as e:
			GUIManager.log("ERROR Terminator - Process Terminator #002:{} \n ".format(e))
		return False

	def isExcluded(self, filesystemEvent):
		try:
			if filesystemEvent.process_id in self.excluded:
				return True
		except Exception as e:
			GUIManager.log("ERROR Terminator - Process Terminator #003:{} \n ".format(e))
		return False
