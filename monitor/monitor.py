'''
This file is part of FrontLine.

FrontLine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FrontLine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FrontLine.  If not, see <https://www.gnu.org/licenses/>.
'''
import ZODB, ZODB.FileStorage, transaction
import persistent
import persistent.list

import threading, asyncio, os, time, sys, queue
import win32pipe, win32file, pywintypes

from pprint import pprint

from monitor.filesystem_events import FilesystemEvents
from monitor.filesystem_event import FilesystemEvent
from monitor.filesystem_logger import FilesystemLogger
from gui_manager import GUIManager

def threaded(fn):
	def wrapper(*args, **kwargs):
		thread = threading.Thread(target=fn, args=args, kwargs=kwargs)
		thread.start()
		return thread
	return wrapper

class Monitor(threading.Thread):
	stop = False
	DB_ENTRY = 'filesystemEvent'
	DB_PATH = './storage/db/'
	eventQueue = queue.Queue()

	logfile = r'\\.\pipe\FL'

	log_queue = queue.Queue()
	TERMINATED = False

	def __init__(self, driveMapper, fz_controller, sz_controller, hz_controller, terminator):
		threading.Thread.__init__(self)
		self.terminator = terminator
		self.filesystemLogger = FilesystemLogger(terminator)
		self.driveMapper = driveMapper
		self.fz_controller = fz_controller
		self.sz_controller = sz_controller
		self.hz_controller = hz_controller
		#self.logfile_sys = self.driveMapper.rmap(self.logfile)
		try:
			self.getDB()
			self.init_db()
			GUIManager.log("£££££££££££££££££££££ Monitor is intitialised £££££££££££££££££££££££££")
			self.intitialised = True
		except Exception as e:
			GUIManager.log("ERROR Monitor __init__#0013: {}".format(e))
		self.create_pipe()

	def getDB(self):
		db_path = '{}/{}.fs'.format(self.DB_PATH, self.DB_ENTRY)
		storage = ZODB.FileStorage.FileStorage(db_path)
		c = threading.Condition()
		db = ZODB.DB(storage)
		self.transaction_manager = transaction.TransactionManager()
		connection = db.open(self.transaction_manager)
		self.root = connection.root()

	def init_db(self):
		if self.exists():
			GUIManager.log("FilesystemEvents Were Found in the DB")
			self.filesystemEvents = self.root[self.DB_ENTRY]
		else:
			GUIManager.log("No FilesystemEvents Were Found in the DB")
			self.root[self.DB_ENTRY] = FilesystemEvents()
			self.filesystemEvents = self.root[self.DB_ENTRY]
			self.transaction_manager.commit()
		self.committer()

	def exists(self):
		return 	self.DB_ENTRY in self.root

	def terminate(self):
		self.filesystemLogger.stop()
		self.stop = True

	@threaded
	def committer(self):
		while not self.stop:
			try:
				self.transaction_manager.commit()
			except KeyboardInterrupt:
				exit()
			except Exception as e:
				GUIManager.log("ERROR Monitor committer#0015: {}".format(e))
				GUIManager.log(e)
			finally:
				time.sleep(2)
				pass

	def run(self):
		self.launch()

	@threaded
	def launch(self):
		while not self.stop:
			try:
				while not self.log_queue.empty() and not self.stop:
					event = self.log_queue.get()
					filesystemEvent = FilesystemEvent(event, self.driveMapper)
					if (filesystemEvent.valid):
						self.dispatch(filesystemEvent)
						self.filesystemEvents.add(filesystemEvent)
				pass
			except KeyboardInterrupt:
				exit()
			except Exception as e:
				GUIManager.log("ERROR Monitor#002: {}".format(e))
			finally:
				time.sleep(1)
				pass

	@threaded
	def dispatch(self, filesystemEvent):
		if (not self.terminator.isWatched(filesystemEvent)) and (not self.terminator.isExcluded(filesystemEvent)) :
			self.fz_controller.addNewEvent(filesystemEvent)
			self.hz_controller.addNewEvent(filesystemEvent)
			self.sz_controller.addNewEvent(filesystemEvent)

	@threaded
	def create_pipe(self):
		while not self.TERMINATED and not self.stop:
			try:
				GUIManager.log("Trying to create pipe ")
				self.pipe = win32pipe.CreateNamedPipe(
					self.logfile, #r'\\.\pipe\Foo',
					win32pipe.PIPE_ACCESS_DUPLEX,
					win32pipe.PIPE_TYPE_MESSAGE | win32pipe.PIPE_READMODE_MESSAGE | win32pipe.PIPE_WAIT,
					1, 65536, 65536,
					0,
					None)
				return_code = self.filesystemLogger.start()
				if (return_code > -1):
					self.start_receiving()
				else:
					GUIManager.log("ERROR: Couldn't Launch the logger ")
					self.TERMINATED = True
			except Exception as e:
				GUIManager.log("ERROR Monitor#003: {}".format(e))

	def start_receiving(self):
		try:
			GUIManager.log("waiting for log")
			win32pipe.ConnectNamedPipe(self.pipe, None)
			count = 0
			undecodable = 0
			quit = False
			while not quit and not self.stop:
				try:
					res = win32pipe.SetNamedPipeHandleState(self.pipe, win32pipe.PIPE_READMODE_MESSAGE, None, None)
					if res == 0:
						GUIManager.log(f"SetNamedPipeHandleState return code: {res}")
					while not self.stop:
						(code, resp) = win32file.ReadFile(self.pipe, 64*1024)
						try:
							self.log_queue.put(resp.decode('utf-8'))
							#GUIManager.log(f"message: {resp}")
							#GUIManager.log(count, end="\r")
							count += 1
							#print(count)
							GUIManager.updateEventCount(count)
						except Exception as ex:
							try:
								self.log_queue.put(resp.decode('latin-1').encode('utf-8'))
							except Exception as e:
								GUIManager.log("ERROR Monitor#006: Not UTF-8, Not Latin-1: {}\n{}".format(ex, resp))
								undecodable += 1
				except pywintypes.error as e:
					GUIManager.log("ERROR Monitor#005: {}".format(e))
					if e.args[0] == 2:
						GUIManager.log("no pipe, trying again in a sec")
						time.sleep(1)
					elif e.args[0] == 109:
						GUIManager.log("broken pipe, bye bye")
						quit = True
					else:
						time.sleep(1)
						GUIManager.log(e)
				GUIManager.log("Message Count = {}".format(count))
		except Exception as e:
			GUIManager.log("ERROR Monitor#004: {}".format(e))
		finally:
			win32file.CloseHandle(self.pipe)
			GUIManager.log("Pipe is closed")
		GUIManager.log("Message Count = {}".format(count))
