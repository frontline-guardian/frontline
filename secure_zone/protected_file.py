'''
This file is part of FrontLine.

FrontLine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FrontLine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FrontLine.  If not, see <https://www.gnu.org/licenses/>.
'''

import persistent
import persistent.list

import time

from agents.win import filesystem
from statistics import statistics


class ProtectedFile(persistent.Persistent):
	def __init__(self,  file, path, extension_stats):
		self.name = file.name
		self.path = path
		self.fullpath = filesystem.fullpath(self.path, self.name)
		self.extension = filesystem.extension(self.name)
		self.attributes = filesystem.attributes(file.stat())
		self.stats = statistics.stats(self.fullpath, extension_stats) if extension_stats else {}
		self.extension_stats = extension_stats
		self.honeyFiles = persistent.list.PersistentList()
		self._p_changed = 1
		print(self.fullpath , end="\r")

	def get_role(original= None, creator= 'user'):
		role = {
			'creator'	:  creator,
			'original'	: original if original else "",
			'backups'	: []
		}
		return role

	def __str__(self):
		text  = "----------------------------" + 'FullPath: {}\n'.format(self.fullpath)
		text += "----------------------------" + 'Path: {}\n'.format(self.path)
		text += "----------------------------" + 'Name: {}\n'.format(self.name)
		text += "----------------------------" + 'Extension: {}\n'.format(self.extension)
		text += "----------------------------" + 'Attributes: {}\n'.format(self.attributes)
		text += "----------------------------" + 'Stats: {}\n'.format(self.stats)
		text += "----------------------------" + 'HoneyFiles: {}\n'.format(self.honeyFiles)
		#text += 'Parents: {}\n'.format(self.parents)

		return text

	def fileList(self):
		return [self.fullpath]

	def fileSize(self):
		return 0 if not self.attributes else self.attributes['st_size']

	def addHoneyFile(self, honeyFile):
		#print("HoneyFile: {} for {}".format(honeyFile, self.fullpath))
		self.honeyFiles.append(honeyFile)
		#print(self.honeyFiles)
		#print("-----------------------------------------")
		self._p_changed = 1

	def newEventReceived(self, event):
		print("Event: {} for {}".format(event, self.fullpath))
		print("-----------------------------------------")

	def isBackupNeeded(self, honeyFile):
		#print("isBackupNeeded: {}".format(self.fullpath))
		return (len(self.honeyFiles) < 1) or (not self.isBackupUpdated(honeyFile))

	def isBackupUpdated(self, honeyFile):
		#print("isBackupUpdated: {}".format(self.fullpath))
		changes = self.compare(honeyFile.attributes)
		return len(changes) < 1

	def compare(self, attributes):
		#print("----------------------------------------------")
		#print(self)
		changes = []
		if attributes['st_size'] != self.attributes['st_size']:
			print("st_size : {}: {} !== {} = {}".format(self.fullpath, self.attributes['st_size'], attributes['st_size'], attributes['st_size'] != self.attributes['st_size']))
			changes += [{'attribute': 'st_size', 'protectedFile': self.attributes['st_size'], 'honeyFile': attributes['st_size']}]
		if attributes['st_mtime'] > self.attributes['st_mtime']:
			print("st_mtime : {}: {} < {} = {} ".format(self.fullpath, self.attributes['st_mtime'], attributes['st_mtime'], attributes['st_mtime'] <= self.attributes['st_mtime']))
			changes += [{'attribute': 'st_mtime', 'protectedFile': self.attributes['st_mtime'], 'honeyFile': attributes['st_mtime']}]
		return changes

	def update(self):
		try:
			self.attributes = filesystem.file_attributes(self.path, self.name)
			self._p_changed = 1
			return True
		except Exception as e:
			return False

	def wasAttacked(self):
		try:
			attributes = filesystem.file_attributes(self.path, self.name)
			changes = self.compare(attributes)
			return len(changes) > 0
		except Exception as e:
			return True

	def restore(self):
		if len(self.honeyFiles) > 0:
			new_backup_fullpath = "{}.bak_{}".format(self.fullpath, str(round(time.time() * 1000)))
			filesystem.move(self.fullpath, new_backup_fullpath)
			filesystem.clone(self.honeyFiles[-1], self.fullpath)
			self.update()
			return True
		return False
