'''
This file is part of FrontLine.

FrontLine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FrontLine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FrontLine.  If not, see <https://www.gnu.org/licenses/>.
'''

import queue, threading, time
from tkinter import *

def threaded(fn):
	def wrapper(*args, **kwargs):
		thread = threading.Thread(target=fn, args=args, kwargs=kwargs)
		thread.start()
		return thread
	return wrapper


class GUIManager:
	logText = None
	eventCount = None
	ransomwareEventCount = None
	ransomwareEventCounter = 0

	ransomwarePrcoessCounter = 0
	ransomwarePrcoessList = None

	excludedPrcoessCounter = 0
	excludedPrcoessList = None

	attackedFileCounter = 0
	attackedFileList = None
	attackedFiles = []

	detailText = None

	@staticmethod
	def log(text):
		GUIManager.logText.insert(END, str(text) + "\n")

	@staticmethod
	def updateEventCount(count):
		GUIManager.eventCount.set(str(count))

	@staticmethod
	@threaded
	def addRansomwareEvent(filesystemEvent):
		GUIManager.ransomwareEventCounter += 1
		GUIManager.ransomwareEventCount.set(str(GUIManager.ransomwareEventCounter))

	@staticmethod
	@threaded
	def addRansomwareProcess(process, process_name, process_id):
		GUIManager.ransomwarePrcoessCounter += 1
		GUIManager.ransomwarePrcoessList.insert(GUIManager.ransomwarePrcoessCounter, "{}:{}@{}".format(process_id, process_name, process))

	@staticmethod
	@threaded
	def addExcludedProcess(process_name, process_id):
		GUIManager.excludedPrcoessCounter += 1
		GUIManager.excludedPrcoessList.insert(GUIManager.excludedPrcoessCounter, "{}:{}".format(process_id, process_name))

	@staticmethod
	@threaded
	def addattackedFile(filesystemEvent):
		if (filesystemEvent not in GUIManager.attackedFiles):
			GUIManager.attackedFiles.append(filesystemEvent)
			GUIManager.attackedFileCounter += 1
			GUIManager.attackedFileList.insert(GUIManager.attackedFileCounter, "{}".format(filesystemEvent.file_fullpath))

	### Events
	def attackedFileListSelected(evt):
		# Note here that Tkinter passes an event object to onselect()
		try:
			w = evt.widget
			index = int(w.curselection()[0])
			value = w.get(index)
			GUIManager.detailText.delete('1.0', END)
			GUIManager.detailText.insert(END, "You selected item {}: {}".format(index, value))
		except Exception as e:
			pass
