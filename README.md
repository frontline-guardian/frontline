# FrontLine

An Open-source decoy-based anti-ransomware, this project is still under development and should be used for testing purposes.  

## Getting Started

### For Testing

#### Prerequisites
```
Windows (other platforms will be supported shortly)
```
```
Python 3.81 and pip3
```
```
git
```

#### Installing
```
git clone https://gitlab.com/frontline-guardian/frontline.git
cd frontline
init.bat
```

## Running the tests

To do ....

## Versioning

To do ....

## Authors
FrontLine Development Team

## License

This project is licensed under the GPLv3 License - see the [LICENSE.md](COPYING.md) file for details

## Acknowledgments
