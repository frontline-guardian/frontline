'''
This file is part of FrontLine.

FrontLine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FrontLine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FrontLine.  If not, see <https://www.gnu.org/licenses/>.
'''

import persistent
import persistent.list

import os, time, json, os.path, copy
from pathlib import Path

from agents.win import filesystem

from honey_zone.directories import Directories

class HoneyZone(persistent.Persistent):
	zones = None
	def __init__(self, path, path_sys, settings, recursive = True):
		self.path = path
		self.path_sys = path_sys
		self.settings = settings
		self.directories = Directories(self.path, self.settings, True, recursive)
		#self.traverse()
		self._p_changed = 1

	def path(self):
		return self.path

	def path_sys(self):
		return self.path_sys

	def settings_get(self):
		pprint(self.settings)
		return self.settings

	def analysis(self, path):
		return self.settings['analysis']

	def role(self, path):
		return self.settings['role']

	def __str__(self):
		text = "-- HoneyZone: {} \n".format(self.path)
		text += "---- Directories: \n" + str(self.directories)
		return text

	def dirList(self):
		return self.directories.dirList()

	def fileList(self):
		return self.directories.fileList()

	def fileSize(self):
		return self.directories.fileSize()

	def addHoneyFile(self, protectedFile, protectedFile_extension_stats, honeyFile):
		try:
			index = honeyFile.find(self.path)
			if index == 0:
				subpathList = honeyFile[len(self.path):].split("\\")
				for item in subpathList:
					if item == '':
						subpathList.remove(item)
				if (len(subpathList) > 0):
					return self.directories.addHoneyFile(protectedFile, protectedFile_extension_stats, honeyFile, subpathList)
				else:
					print("{}: honeyFile name is wrong @Files".format(honeyFile))
			print("{}: honeyFile alread exists".format(honeyFile))
		except Exception as e:
			print("ERROR honey_zone addHoneyFile#0001: {}".format(e))
		finally:
			return None

	def whatChanged(self, honeyFile):
		index = honeyFile.find(self.path)
		if index == 0:
			subpathList = honeyFile[len(self.path):].split("\\")
			for item in subpathList:
				if item == '':
					subpathList.remove(item)
			if (len(subpathList) > 0):
				self.directories.whatChanged(subpathList)
