'''
This file is part of FrontLine.

FrontLine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FrontLine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FrontLine.  If not, see <https://www.gnu.org/licenses/>.
'''

import persistent
import persistent.list

from pprint import pprint

from honey_zone.honey_file import HoneyFile

class HoneyFiles(persistent.Persistent):
	def __init__(self):
		self.honeyFiles = persistent.list.PersistentList()
		self._p_changed = 1

	def add(self, honeyFile):
		self.honeyFiles.append(honeyFile)
		self._p_changed = 1

	def list(self):
		return self.honeyFiles

	def get(self, path):
		for honeyFile in self.honeyFiles:
			if honeyFile.path().upper() == path.upper():
				return honeyFile
		return None

	def __str__(self):
		text = ""
		for honeyFile in self.honeyFiles:
			text +=  str(honeyFile) + "\n"
		return text

	def fileList(self):
		list = []
		for honeyFile in self.honeyFiles:
			list += [{'fullpath': honeyFile.fullpath, 'object_ref': honeyFile}]
		return list

	def fileSize(self):
		size = 0
		for honeyFile in self.honeyFiles:
			size += honeyFile.fileSize()
		return size

	def addHoneyFile(self, protectedFile, protectedFile_extension_stats, honeyFilePath, honeyFileName):
		try:
			honeyFileObject = HoneyFile(honeyFileName, honeyFilePath, protectedFile, protectedFile_extension_stats)
			self.add(honeyFileObject)
			return honeyFileObject
		except Exception as e:
			print("ERROR honey_files addHoneyFile#0001: {}".format(e))
		finally:
			return None

	def whatChanged(self, honeyFileName):
		honeyFile = self.get(honeyFileName)
		if honeyFile:
			honeyFile.whatChanged()
