'''
This file is part of FrontLine.

FrontLine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FrontLine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FrontLine.  If not, see <https://www.gnu.org/licenses/>.
'''

import ZODB, ZODB.FileStorage, transaction
import persistent
import persistent.list

import queue, threading, time

from secure_zone.secure_zones import SecureZones
from utils.color import Color
from gui_manager import GUIManager

def threaded(fn):
	def wrapper(*args, **kwargs):
		thread = threading.Thread(target=fn, args=args, kwargs=kwargs)
		thread.start()
		return thread
	return wrapper

class SZ_Controller(threading.Thread):
	stop = False
	DB_ENTRY = 'secureZones'
	DB_PATH = './storage/db/'
	SECURE_ZONE_FILE = '\\storage\\secure_zones.json'
	requestQueue = queue.Queue()
	eventQueue = queue.Queue()

	def __init__(self, conf_root, driveMapper):
		threading.Thread.__init__(self)
		self.intitialised = False
		try:
			self.getDB()
			self.init_db(conf_root, driveMapper)
			GUIManager.log("£££££££££££££££££££££ SZ_Controller is intitialised £££££££££££££££££££££££££")
			self.intitialised = True
		except Exception as e:
				GUIManager.log("ERROR SZ __init__#0014: {}".format(e))

	def getDB(self):
		db_path = '{}/{}.fs'.format(self.DB_PATH,self.DB_ENTRY)
		storage = ZODB.FileStorage.FileStorage(db_path)
		c = threading.Condition()
		db = ZODB.DB(storage)
		self.transaction_manager = transaction.TransactionManager()
		connection = db.open(self.transaction_manager)
		self.root = connection.root()

	def init_db(self, conf_root, driveMapper):
		if self.exists():
			GUIManager.log("SecureZones Were Found in the DB")
			self.secureZones = self.root[self.DB_ENTRY]
			#GUIManager.log(self.secureZones)
		else:
			GUIManager.log("No SecureZones Were Found in the DB")
			self.root[self.DB_ENTRY] = SecureZones(conf_root, driveMapper)
			self.secureZones = self.root[self.DB_ENTRY]
			self.transaction_manager.commit()
		self.fileList()
		self.fileListUpdater()
		self.eventReceiver()
		self.committer()

	def exists(self):
		return 	self.DB_ENTRY in self.root

	def zones(self):
		return self.secureZones.zones()

	def fileList(self, namingRules = False):
		self.protectedFileList = self.secureZones.fileList(namingRules)
		return self.protectedFileList

	def fileCount(self):
		return len(self.fileList())

	def fileSize(self):
		return self.secureZones.fileSize()

	def addRequest(self, request):
		self.requestQueue.put(request)

	def isProtectedFile(self, file_fullpath):
		result = next((file for file in self.protectedFileList if file["fullpath"] == file_fullpath), None)
		return result

	def addNewEvent(self, event):
		self.eventQueue.put(event)

	def whatChanged(self, protectedFile):
		return self.secureZones.whatChanged(protectedFile)

	def restore(self, protectedFile):
		restored = protectedFile.restore()
		#self.transaction_manager.commit()
		return restored

	def getProtectedFile(self, protectedFile_FullPath):
		for item in self.protectedFileList:
			if item["fullpath"] == protectedFile_FullPath:
				return item
		return None

	def terminate(self):
		self.stop = True

	@threaded
	def eventReceiver(self):
		while not self.stop:
			while not self.eventQueue.empty() and not self.stop:
				try:
					event = self.eventQueue.get()
					if self.isProtectedFile(event.file_fullpath):
						GUIManager.log("{} needs to be updated".format(event.file_fullpath))
						protectedFile = self.getProtectedFile(event.file_fullpath)
						success = protectedFile['object_ref'].update()
						#GUIManager.log(Color.red(str(event)))
				except KeyboardInterrupt:
					exit()
				except Exception as e:
					GUIManager.log("ERROR SZ requestQueue_monitor#0011: {}".format(e))
					GUIManager.log(event)
			else:
				time.sleep(0.5)

	@threaded
	def committer(self):
		while not self.stop:
			try:
				self.transaction_manager.commit()
			except KeyboardInterrupt:
				exit()
			except Exception as e:
				GUIManager.log("ERROR HZ requestQueue_monitor#0015: {}".format(e))
				#GUIManager.log(e)
			finally:
				time.sleep(2)
				pass

	@threaded
	def fileListUpdater(self):
		while not self.stop:
			try:
				self.fileList()
			except KeyboardInterrupt:
				exit()
			except Exception as e:
				GUIManager.log("ERROR SZ requestQueue_monitor#0012: {}".format(e))
				GUIManager.log(e)
			finally:
				time.sleep(10)
				pass

	def run(self):
		while not self.stop:
			try:
				while not self.requestQueue.empty() and not self.stop:
					request = self.requestQueue.get()
					self.dispatch(request)
			except KeyboardInterrupt:
				exit()
			except Exception as e:
				GUIManager.log("ERROR SZ requestQueue_monitor#002: {}".format(e))
			finally:
				time.sleep(1)
				pass

	def dispatch(self, request):
		#GUIManager.log(request)
		function = request['function']
		data = request['data']
		try:
			if function == "addHoneyFile":
				#GUIManager.log("New HoneyFile has been added: {}".format(newHoneyFile))
				self.secureZones.addHoneyFile(data['protectedFile'], data['honeyFile'])
			elif function == "updateProtectedFile":
				self.secureZones.updateProtectedFile(data['protectedFile'], data['update'])
			elif function == "newEventReceived":
				self.secureZones.newEventReceived(data['protectedFile'], data['event'])
			else:
				pass

		except Exception as e:
			GUIManager.log("ERROR requestQueue_monitor#001: {}".format(e))
