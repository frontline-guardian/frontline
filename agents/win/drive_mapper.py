'''
This file is part of FrontLine.

FrontLine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FrontLine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FrontLine.  If not, see <https://www.gnu.org/licenses/>.
'''

import subprocess, shlex, _thread, time
from pprint import pprint
from gui_manager import GUIManager

class DriveMapper():
	drive_mapper = 'powershell -executionPolicy bypass -f agents/win/drive_mapper.ps1'
	drive_map = {}
	rdrive_map = {}

	def __init__(self):
		self.scan()

	def scan(self):
		GUIManager.log("---------------- DriveMapper Init -----------------")
		args= shlex.split(DriveMapper.drive_mapper)
		with subprocess.Popen(args, stdout=subprocess.PIPE) as proc:
			output = proc.stdout.read()
		output_str = output.decode('utf-8')
		output_array = output_str.split()
		output_array.pop(3)
		output_array.pop(2)
		output_array.pop(1)
		output_array.pop(0)
		index = 0
		while index < len(output_array):
			key = bytes(output_array[index], 'utf-8').decode() #decode('unicode_escape')
			GUIManager.log(key)
			if '\\Device\\CdRom' not in key:
				DriveMapper.drive_map[key] = output_array[index+1]
				DriveMapper.rdrive_map[output_array[index+1]] = key
			index += 2
		GUIManager.log(DriveMapper.drive_map)

	@staticmethod
	def list():
		return DriveMapper.drive_map

	@staticmethod
	def rlist():
		return DriveMapper.rdrive_map

	@staticmethod
	def map(file_path):
		for path in DriveMapper.drive_map:
			file_path = file_path.replace(path, DriveMapper.drive_map[path])
		return file_path

	@staticmethod
	def rmap(file_path):
		for path in DriveMapper.rdrive_map:
			file_path = file_path.replace(path, DriveMapper.rdrive_map[path])
		return file_path
