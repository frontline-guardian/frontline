'''
This file is part of FrontLine.

FrontLine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FrontLine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FrontLine.  If not, see <https://www.gnu.org/licenses/>.
'''

import tkinter as tk
from tkinter import *

import time, os, sys

from gui_manager import GUIManager
from agents.platform import platform
from agents.win.drive_mapper import DriveMapper
from forbidden_zone.fz_controller import FZ_Controller
from secure_zone.sz_controller import SZ_Controller
from honey_zone.hz_controller import HZ_Controller
from honey_token.ht_controller import HT_Controller
from monitor.monitor import Monitor
from utils.terminator import Terminator

class FrontLine(tk.Frame):
	started = False
	colours = ['red','green','orange','white','yellow','blue']

	def __init__(self, master=None):
		tk.Frame.__init__(self, master)
		self.grid()
		self.createWidgets()

	def start_frontline(self):
		print("*************************************************************")
		print("* Frontline has just started running on {} OS".format(platform()))
		print("*************************************************************")
		self.rootDirectory = os.getcwd()
		self.driveMapper = DriveMapper()
		self.terminator = Terminator()
		self.fz_controller = FZ_Controller(self.driveMapper, self.terminator)
		self.fz_controller.start()
		self.sz_controller = SZ_Controller(self.rootDirectory, self.driveMapper)
		self.sz_controller.start()
		self.hz_controller = HZ_Controller(self.rootDirectory, self.driveMapper, self.terminator)
		self.hz_controller.start()
		self.ht_controller = HT_Controller(self.sz_controller, self.hz_controller)
		self.ht_controller.start()
		self.monitor = Monitor(self.driveMapper, self.fz_controller, self.sz_controller, self.hz_controller, self.terminator)
		self.monitor.start()

	#### Widgets
	def createWidgets(self):
		#self.createWidget_status()
		self.createWidget_start(self)
		self.createWidget_quit(self)
		self.createWidget_eventCounter(self)
		self.createWidget_ransomwareEventCounter(self)
		self.createWidget_excludedPrcoessList(self)
		self.createWidget_ransomwarePrcoessList(self)
		self.createWidget_attackedFileList(self)
		self.createWidget_log(self)
		self.createWidget_detailText(self)

	def createWidget_start(self, pane):
		self.toggleButton = tk.Button(pane, text='Start', command=self.toggleButtonAction)
		self.toggleButton.grid(row=0, column=0, sticky=N+S+E+W)

	def createWidget_quit(self, pane):
		self.quitButton = tk.Button(pane, text='Quit', command=self.quitCommand)
		self.quitButton.grid(row=0, column=1,  sticky=N+S+E+W)

	def createWidget_eventCounter(self, pane):
		self.eventCounterLabel = tk.Label(pane, text="All Events", relief=tk.RIDGE, width=15)
		self.eventCounterLabel.grid(row=1,column=0, sticky=N+S+E+W)
		GUIManager.eventCount = tk.StringVar()
		GUIManager.eventCount.set("0")
		self.eventCounter = tk.Label(pane, textvariable=GUIManager.eventCount, relief=tk.RIDGE, width=15)
		self.eventCounter.grid(row=1,column=1, sticky=N+S+E+W)

	def createWidget_ransomwareEventCounter(self, pane):
		self.ransomwareEventCounterLabel = tk.Label(pane, text="Ransomware Events", relief=tk.RIDGE, width=15)
		self.ransomwareEventCounterLabel.grid(row=2,column=0,sticky=N+S+E+W)
		GUIManager.ransomwareEventCount = tk.StringVar()
		GUIManager.ransomwareEventCount.set("0")
		self.ransomwareEventCounter = tk.Label(pane, textvariable=GUIManager.ransomwareEventCount, relief=tk.RIDGE, width=15)
		self.ransomwareEventCounter.grid(row=2,column=1, sticky=N+S+E+W)


	def createWidget_excludedPrcoessList(self, pane):
		self.excludedPrcoessList = tk.Listbox(pane ,height= 12)
		self.excludedPrcoessList.grid(row=3, column=7, columnspan=3, sticky=N+S+E+W)
		GUIManager.excludedPrcoessList = self.excludedPrcoessList

	def createWidget_ransomwarePrcoessList(self, pane):
		self.ransomwarePrcoessList = tk.Listbox(pane,height= 12, width=40, selectmode=SINGLE)
		self.ransomwarePrcoessList.grid(row=3, column=0, columnspan=7, sticky=N+S+E+W)
		GUIManager.ransomwarePrcoessList = self.ransomwarePrcoessList

	def createWidget_attackedFileList(self, pane):
		self.attackedFileList = tk.Listbox(pane,height= 12, width=40, selectmode=SINGLE)
		self.attackedFileList.bind('<<ListboxSelect>>', GUIManager.attackedFileListSelected)
		self.attackedFileList.grid(row=4, column=0, columnspan=10, sticky=N+S+E+W)
		GUIManager.attackedFileList = self.attackedFileList

	def createWidget_status(self):
		r = 0
		for c in self.colours:
			tk.Label(self, text=c, relief=tk.RIDGE, width=15).grid(row=r,column=0, sticky=N+S+E+W)
			tk.Entry(self, bg=c, relief=tk.SUNKEN, width=10).grid(row=r,column=1, sticky=N+S+E+W)
			r = r + 1

	def createWidget_log(self, pane):
		self.logText = tk.Text(pane, height=7)
		self.logText.grid(row=5,column=0, columnspan=10, sticky=N+S+E+W)
		GUIManager.logText = self.logText

	def createWidget_detailText(self, pane):
		self.detailText = tk.Text(pane, height=25)
		self.detailText.grid(row=3, rowspan=2, column=10, columnspan=10, sticky=N+S+E+W)
		GUIManager.detailText = self.detailText

	### Commands
	def quitCommand(self):
		self.stopCommand()
		os._exit(0)

	def stopCommand(self):
		try:
			self.monitor.terminate()
			self.fz_controller.terminate()
			self.sz_controller.terminate()
			self.hz_controller.terminate()
		except Exception as e:
			pass

	def toggleButtonAction(self):
		if self.started:
			GUIManager.log('Will be stopped')
			self.toggleButton["text"] = 'Start'
		else:
			GUIManager.log('Will be started')
			self.toggleButton["text"] = 'Stop'
			self.start_frontline()
		self.started = not self.started

if __name__ == "__main__":
	app = FrontLine()
	app.master.title('FrontLine')
	#app.master.geometry("800x600")
	#app.master.resizable(0, 0)
	column, row = app.master.grid_size()
	print("Column: {} - Row: {}".format(column, row))
	app.mainloop()
