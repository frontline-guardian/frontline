import ZODB, ZODB.FileStorage, transaction
import persistent
import persistent.list

import threading, asyncio, os, time, sys, queue
from pprint import pprint

from monitor.monitor import Monitor

class Analyser(threading.Thread):
	DB_ENTRY = 'filesystemEvent'
	DB_PATH = './storage/db/'
	def __init__(self):
		threading.Thread.__init__(self)
		try:
			self.getDB()
			self.init_db()
		except Exception as e:
			print("ERROR Monitor __init__#0013: {}".format(e))

	def getDB(self):
		db_path = '{}/{}.fs'.format(self.DB_PATH, self.DB_ENTRY)
		storage = ZODB.FileStorage.FileStorage(db_path)
		c = threading.Condition()
		db = ZODB.DB(storage)
		self.transaction_manager = transaction.TransactionManager()
		connection = db.open(self.transaction_manager)
		self.root = connection.root()

	def init_db(self):
		if self.exists():
			print("FilesystemEvents Were Found in the DB")
			self.filesystemEvents = self.root[self.DB_ENTRY]
			return True
		else:
			return False

	def exists(self):
		return 	self.DB_ENTRY in self.root

	def analyse(self):
		if (self.exists()):
			ransomware_events = self.filesystemEvents.getRansomware()
			benign_events = self.filesystemEvents.getBenign()
			ignored_events = self.filesystemEvents.getIgnored()
			excluded_events = self.filesystemEvents.getExcluded()

			byFilePath = self.filesystemEvents.byFilePath("D:\\Documents\\bkp.20200225_101117177602.bin")
			for event in byFilePath:
				print(event)
			print("************************************")
			'''
			for event in excluded_events:
				print(event)
			'''
			print("Ransomware: {}, Benign: {}, Excluded: {}, Ignored: {}".format(len(ransomware_events), len(benign_events), len(excluded_events), len(ignored_events)))

if __name__ == "__main__":
	analyser = Analyser()
	analyser.analyse()
