'''
This file is part of FrontLine.

FrontLine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FrontLine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FrontLine.  If not, see <https://www.gnu.org/licenses/>.
'''

import time, secrets, datetime, random, string
from pprint import pprint
from agents.win import filesystem

class PathGenerator:
	def __init__(self, hz_directories):
		self.hz_directories = hz_directories

	def generate(self, original_filepath, filename):
		exists = True
		path = ""
		total = 0
		while exists and total < len(self.hz_directories):
			index = secrets.randbelow(len(self.hz_directories))
			path = self.hz_directories[index]
			fullpath = filesystem.fullpath(path, filename)
			exists = filesystem.path_exists(path)
			total += 1
		else:
			index = secrets.randbelow(len(self.hz_directories))
			path = self.hz_directories[index]
		return path
