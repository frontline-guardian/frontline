'''
This file is part of FrontLine.

FrontLine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FrontLine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FrontLine.  If not, see <https://www.gnu.org/licenses/>.
'''

import persistent
import persistent.list

import os, time, json, os.path, copy
from pathlib import Path
from pprint import pprint

from agents.win import filesystem
from secure_zone.extensions import Extensions
from secure_zone.directories import Directories

class ForbiddenZone(persistent.Persistent):
	zones = None
	def __init__(self, path, path_sys, settings):
		self.path = path
		self.path_sys = path_sys
		self.settings = settings
		self._p_changed = 1

	def path(self):
		return self.path

	def path_sys(self):
		return self.path_sys

	def settings_get(self):
		pprint(self.settings)
		return self.settings

	def __str__(self):
		text = "-- ForbiddenZone: {} \n".format(self.path)
		return text
