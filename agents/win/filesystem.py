'''
This file is part of FrontLine.

FrontLine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FrontLine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FrontLine.  If not, see <https://www.gnu.org/licenses/>.
'''

import os, os.path, time, string, shutil, ntpath
from pathlib import Path
from pprint import pprint
from statistics import statistics

current_milli_time = lambda :int(round(time.time() * 1000))
SYSTEM_DIRECTORIES = ['$Recycle.Bin', 'System Volume Information']

def cwd():
	return os.getcwd()

def expanduser(path):
	return os.path.expanduser(path)

def filename(path):
	return ntpath.basename(path)

def dirname(path):
	return os.path.dirname(path)

def extension(name):
	return Path(name).suffix

def fullpath(path, name):
	return os.path.join(path, name)

def isfile(path, name = ""):
	return os.path.isfile(os.path.join(path, name))

def isdir(path, name = ""):
	return os.path.isdir(os.path.join(path, name))

def scandir(path):
	try:
		return os.scandir(path)
	except Exception as e:
		return []

def get_directories(path, no_shortcuts= True):
	directories = [f for f in scandir(path) if isdir(path, f)]
	return filter_shortcuts(path, directories) if no_shortcuts else directories

def get_files(path):
	return [f for f in scandir(path) if isfile(path, f)]

def filter_shortcuts(path, directories):
	to_delete = []
	for directory in directories:
		try:
			subdirectories = [d for d in scandir(os.path.join(path, directory.name))]
			if (directory.name.upper() in (system_directory.upper() for system_directory in SYSTEM_DIRECTORIES)):
				to_delete.append(directory)
		except Exception as e:
			to_delete.append(directory)
	for item in to_delete:
		directories.remove(item)
	return directories

def file_attributes(path, name):
	file = [f for f in os.scandir(path) if f.name == name][0]
	return attributes(file.stat())

def attributes(stats):
	local_current_milli_time = current_milli_time()

	attrs = {}
	attrs['st_mode']	= stats.st_mode
	attrs['st_ino']		= stats.st_ino
	attrs['st_nlink']   = stats.st_nlink
	attrs['st_uid']	 	= stats.st_uid
	attrs['st_gid']	 	= stats.st_gid
	attrs['st_size']	= stats.st_size
	#attrs['st_atime']   = stats.st_atime
	attrs['st_mtime']   = stats.st_mtime
	attrs['st_ctime']   = stats.st_ctime
	return attrs

def clone(protectedFile, honeyFile):
	try:
		shutil.copy2(protectedFile, honeyFile, follow_symlinks=False)
		return True
	except Exception as ex:
		print(ex)
		return False, ex

def path_exists(path):
	try:
		files = [f for f in os.scandir(path)]
		return True
	except Exception as e:
		return False

def file_exists(path, name = ""):
	path = dirname(path)
	name = name if name else filename(path)
	files = [f for f in os.scandir(path) if f.name == name]
	return True if len(files) > 0 else False

def move(old_path, new_path):
	done = True
	try:
		os.rename(old_path, new_path)
	except Exception as e:
		done = False
	return done

def delete(path):
	try:
		os.remove(path)
		return True
	except Exception as e:
		return False

'''
def restore(file, backup= None):
	try:
		if backup == None:
			backup = file['role']['backups'][0]
		new_path = file['fullpath'] + "_altered_" + str(current_milli_time())
		move(file['fullpath'], new_path)
		delete(file['fullpath'])
		shutil.copy2(os.path.join(backup['path'], backup['name'] + backup['extension']), file['fullpath'], follow_symlinks=False)
		return True
	except Exception as e:
		return False
'''
