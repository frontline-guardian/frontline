'''
This file is part of FrontLine.

FrontLine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FrontLine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FrontLine.  If not, see <https://www.gnu.org/licenses/>.
'''

import threading, asyncio, os, time, sys, queue
import win32pipe, win32file, pywintypes
import subprocess, psutil

from pprint import pprint

def threaded(fn):
	def wrapper(*args, **kwargs):
		thread = threading.Thread(target=fn, args=args, kwargs=kwargs)
		thread.start()
		return thread
	return wrapper

class FilesystemLogger(threading.Thread):

	logfile = r'\\.\pipe\FL'

	logger_launcher = "agents\\win\\dbgview\launch.bat"
	log_command = r'agents\win\dbgview\Dbgview.exe /v /l "\\.\pipe\FL" /p /g /k /f'
	minifilter_start_command = "net start EventCount"
	minifilter_stop_command = "net stop EventCount"
	log_queue = queue.Queue()
	TERMINATED = False
	logger_pid = -1
	minifilter_pid = -1
	def __init__(self, terminator):
		self.terminator = terminator

	def start(self):
		try:
			self.stop()
			return_code = self.start_minifilter()
			if return_code > -1:
				SW_HIDE = 0
				info = subprocess.STARTUPINFO()
				info.dwFlags = subprocess.STARTF_USESHOWWINDOW
				info.wShowWindow = SW_HIDE
				log_viewer = subprocess.Popen(self.log_command, startupinfo=info)
				self.logger_pid = log_viewer.pid
				self.terminator.exclude(self.logger_pid, parent=False, children=True, recursive=True)
				return self.logger_pid
			else:
				return -1
		except Exception as e:
			print("ERROR Logger#0001: {}".format(e))
			return -1

	def stop(self):
		try:
			return_code = self.stop_minifilter()
			SW_HIDE = 0
			info = subprocess.STARTUPINFO()
			info.dwFlags = subprocess.STARTF_USESHOWWINDOW
			info.wShowWindow = SW_HIDE
			log_viewer = subprocess.Popen("taskkill /IM Dbgview.exe -F", startupinfo=info)
			log_viewer.communicate()
		except Exception as e:
			print("ERROR Logger#0004: {}".format(e))

	def start_minifilter(self):
		try:
			SW_HIDE = 0
			info = subprocess.STARTUPINFO()
			info.dwFlags = subprocess.STARTF_USESHOWWINDOW
			info.wShowWindow = SW_HIDE
			minifilter = subprocess.Popen(self.minifilter_start_command, startupinfo=info)
			self.minifilter_pid = minifilter.pid
			self.terminator.exclude(self.minifilter_pid, parent=False, children=True, recursive=True)
			return self.minifilter_pid
		except Exception as e:
			print("ERROR Logger#0002: {}".format(e))
			return -1

	def stop_minifilter(self):
		try:
			SW_HIDE = 0
			info = subprocess.STARTUPINFO()
			info.dwFlags = subprocess.STARTF_USESHOWWINDOW
			info.wShowWindow = SW_HIDE
			minifilter = subprocess.Popen(self.minifilter_stop_command, startupinfo=info)
		except Exception as e:
			print("ERROR Logger#0003: {}".format(e))
