'''
This file is part of FrontLine.

FrontLine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FrontLine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FrontLine.  If not, see <https://www.gnu.org/licenses/>.
'''

import persistent
import persistent.list
from agents.win import filesystem
from statistics import statistics
from secure_zone.protected_files import ProtectedFiles
from secure_zone.protected_file import ProtectedFile


class ProtectedExtension(persistent.Persistent):
	def __init__(self,  extension, extension_data):
		self.extension = extension.upper()
		self.addStats(extension_data['stats'])
		self.processNamingRules(extension_data['naming'])
		self.protectedFiles = ProtectedFiles()
		self._p_changed = 1

	def addStats(self, stats):
		self.stats = persistent.list.PersistentList()
		for stat in stats:
			self.stats.append(stat)
		self._p_changed = 1

	def processNamingRules(self, namingRules):
		self.namingRules = {}
		self.addRules('templates', namingRules['templates'])
		self.addRules('extensions', namingRules['extensions'])
		self.addRules('separators', namingRules['separators'])

	def addRules(self, rule, items):
		self.namingRules[rule] = persistent.list.PersistentList()
		for item in items:
			self.namingRules[rule].append(item)
		self._p_changed = 1

	def add(self, file, path):
		protectedFile = ProtectedFile(file, path, self.stats)
		self.protectedFiles.add(protectedFile)
		self._p_changed = 1

	def list(self):
		return self.protectedFiles.list()

	def getProtectedFile(self, path):
		return self.protectedFiles.get(path)

	def __str__(self):
		text = "-------------------- Extension: {}\n".format(self.extension)

		text += "----------------------- Stats: \n"
		for stat in self.stats:
			text += "----------------------------" + str(stat) + "\n"

		text += "----------------------- Naming: \n"
		for naming in self.namingRules:
			text += "----------------------------{}: {}\n".format(str(naming), str(self.namingRules[naming]))

		text += "----------------------- Files: \n"
		text += str(self.protectedFiles)
		return text

	def fileList(self, namingRules = False):
		list = self.protectedFiles.fileList()
		return list if not namingRules else self.addNamingRules(list)

	def fileSize(self):
		return self.protectedFiles.fileSize()

	def addNamingRules(self, list):
		for item in list:
			item['namingRules'] =  self.namingRules
		return list

	def addHoneyFile(self, protectedFileName, honeyFile):
		self.protectedFiles.addHoneyFile(protectedFileName, honeyFile)

	def newEventReceived(self, protectedFileName, event):
		self.protectedFiles.newEventReceived(protectedFileName, event)
