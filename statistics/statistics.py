'''
This file is part of FrontLine.

FrontLine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FrontLine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FrontLine.  If not, see <https://www.gnu.org/licenses/>.
'''
import json, collections, numpy

#from entropy import shannon_entropy
from scipy.stats import chisquare, pearsonr, entropy
from pprint import pprint

def prepare_json(data):
	if (len(data) < 4):
		return json.dumps({}, indent=4)

	data_dict = {
		"entropy": data[1],
		"chi-square": data[2],
		"serial-byte-correlation": data[3]
	}
	return json.dumps(data_dict, indent=4)

def serial_byte_correlation(data, byte_values):
	bytes_from_zero = []
	bytes_from_one = []

	for b in byte_values[:-1]:
		bytes_from_zero.append(b)

	for b in byte_values[1:]:
		bytes_from_one.append(b)

	return pearsonr(bytes_from_zero, bytes_from_one)[0]

def chi_square(data, byte_values):
	counter = collections.Counter(byte_values)
	return chisquare(list(counter.values()))[0]

def shannon_entropy(data, byte_values, base=None):
	value,counts = numpy.unique(byte_values, return_counts=True)
	return entropy(counts, base=base)

def stats(path, stat_functions):
	byte_values = []
	stats = {}
	try:
		with open(path, "rb") as file:
			data = file.read()
		for char in data:
			byte_values.append(char)

		for stat_function in stat_functions:
			try:
				stats[stat_function] = globals()[stat_function](data, byte_values)
			except Exception as ex1:
				stats[stat_function] = None

	except Exception as ex:
		print("Ex:" + str(ex))

	return stats

	#pprint(results)
	#return results
