'''
This file is part of FrontLine.

FrontLine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FrontLine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FrontLine.  If not, see <https://www.gnu.org/licenses/>.
'''

import persistent, persistent.list

import time

from utils.color import Color

class Classification(persistent.Persistent):

	def __init__(self, source, result, event_timestamp):
		self.source = source
		self.result = result
		self.timestamp = int(round(time.time() * 1000))
		self.delay = self.timestamp - event_timestamp
		self._p_changed = 1

	def getResult(self):
		if self.result == 'ransomware':
			return Color.red(self.result)
		if self.result == 'excluded':
			return Color.green(self.result)
		if self.result == 'ignored':
			return Color.blue(self.result)
		else:
			return Color.white(self.result)

	def __str__(self):
		text =  'Timestamp: {} / {}ms , {}: {} \n'.format(self.timestamp, self.delay, self.source, self.getResult())
		return text
