'''
This file is part of FrontLine.

FrontLine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FrontLine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FrontLine.  If not, see <https://www.gnu.org/licenses/>.
'''

import persistent
import persistent.list

import json

from agents.win import filesystem
from monitor.filesystem_event import FilesystemEvent
from utils.color import Color

class FilesystemEvents(persistent.Persistent):

	def __init__(self):
		self.filesystemEvents = persistent.list.PersistentList()

	def add(self, filesystemEvent):
		self.filesystemEvents.append(filesystemEvent)
		self._p_changed = 1

	def filterClassification(self, result):
		events = []
		for filesystemEvent in self.filesystemEvents:
			classifications = next((classification for classification in filesystemEvent.classifications if classification.result == result), None)
			if classifications:
				events.append(filesystemEvent)
		return events

	def getRansomware(self):
		return self.filterClassification('ransomware')

	def getBenign(self):
		return self.filterClassification('benign')

	def getIgnored(self):
		return self.filterClassification('ignored')

	def getExcluded(self):
		return self.filterClassification('excluded')

	def byFilePath(self, file_fullpath):
		#events = next((event for event in self.filesystemEvents if event.file_fullpath == file_fullpath), [])
		events = []
		for filesystemEvent in self.filesystemEvents:
			try:
				if filesystemEvent.file_fullpath.upper() == file_fullpath.upper():
					events.append(filesystemEvent)
			except:
				pass
		return events

	def byProcessName(self, process_name):
		#events = next((event for event in self.filesystemEvents if event.file_fullpath == file_fullpath), [])
		events = []
		for filesystemEvent in self.filesystemEvents:
			try:
				if filesystemEvent.process.upper().endswith(process_name.upper()):
					events.append(filesystemEvent)
			except:
				pass
		return events

	def filter(self, **filters):
		events = []
		for attribute,value in filters.items():
			print("{} =? {}".format(attribute, value))

		for filesystemEvent in self.filesystemEvents:
			try:
				valid = 0
				print(filesystemEvent)
				for attribute,value in filters.items():
					print("{} =? {}: {}".format(attribute, value, filesystemEvent[attribute]))
					valid += (1 if (filesystemEvent[attribute] == value) else 0)
				if valid == len(filters):
					events.append(filesystemEvent)
			except:
				pass
		return events

	def __str__(self):
		text = ""
		for filesystemEvent in self.filesystemEvents:
			text +=  '{}\n'.format(str(filesystemEvent))
		return text
