'''
This file is part of FrontLine.

FrontLine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FrontLine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FrontLine.  If not, see <https://www.gnu.org/licenses/>.
'''

import threading, time, queue

class HZ_AnomalyDetector(threading.Thread):
	IDENTIFIER = "HZ_Controller_Anomaly_Detector"
	searchhostprotocol = 'C:\\WINDOWS\\SYSTEM32\\SEARCHHOSTPROTOCOL.EXE'
	windows = 'C:\\WINDOWS'
	explorer = 'C:\\WINDOWS\\EXPLORER.EXE'
	python = 'C:\\PROGRAM FILES\\PYTHON38\\PYTHON.EXE'
	frontline = 'C:\\PROGRAM FILES\\FRONTLINE\\FRONTLINE.EXE'
	excluded_processes = {
		explorer: ['QI', 'RD', 'CL', 'CR'],
		frontline: ['*'],
		python: ['*'],
		searchhostprotocol: ['QI', 'RD']
	}
	excluded_process_ids = {
		"4": ['QI', 'RD', 'CL']
	}

	change_operations = ['WR', 'SI', 'CR']
	eventQueue = queue.Queue()

	def __init__(self, terminator):
		threading.Thread.__init__(self)
		self.terminator = terminator

	def run(self):
		self.checker()

	def checker(self):
		while True:
			while not self.eventQueue.empty():
				try:
					event = self.eventQueue.get()
					self.check(event)
				except KeyboardInterrupt:
					exit()
				except Exception as e:
					print("ERROR HZ_AnomalyDetector#0002: {}".format(e))
			else:
				time.sleep(0.5)

	def check(self, event):
		try:
			if not self.isProcessExcluded(event):
				self.terminator.add(event, self.IDENTIFIER)
			else:
				event.excluded(self.IDENTIFIER)
		except Exception as e:
			print("ERROR HZ_AnomalyDetector#001: {}\n".format(e))

	def submit(self, event):
		try:
			self.eventQueue.put(event)
		except Exception as e:
			print("ERROR HZ_AnomalyDetector#004: {}\n".format(e))

	def isProcessExcluded(self, event):
		try:
			if event.process.upper() in self.excluded_processes :
				ops = self.excluded_processes[event.process.upper()]
				return event.operation in ops or '*' in ops
			elif event.process_id in self.excluded_process_ids:
				ops = self.excluded_process_ids[event.process_id]
				return event.operation in ops or '*' in ops
		except Exception as e:
			print("ERROR HZ_AnomalyDetector#0003:{}\n".format(e))
		return False
