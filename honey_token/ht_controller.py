'''
This file is part of FrontLine.

FrontLine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FrontLine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FrontLine.  If not, see <https://www.gnu.org/licenses/>.
'''

import persistent
import persistent.list

import threading, time

from secure_zone.secure_zones import SecureZones
from honey_token.name_generator import NameGenerator
from honey_token.path_generator import PathGenerator
from agents.win import filesystem
from gui_manager import GUIManager

def threaded(fn):
	def wrapper(*args, **kwargs):
		thread = threading.Thread(target=fn, args=args, kwargs=kwargs)
		thread.start()
		return thread
	return wrapper

class HT_Controller(persistent.Persistent):
	hz_directories = []
	sz_files = []

	def __init__(self, sz_controller, hz_controller):
		self.sz_controller = sz_controller
		self.hz_controller = hz_controller
		self.nameGenerator = NameGenerator()

	@threaded
	def start(self):
		while not (self.sz_controller.intitialised and self.hz_controller.intitialised):
			time.sleep(2)
		self.get_protectedFiles()
		self.get_honeyDirectories()
		self.check_backups()
		self.file_attacked()

	def get_protectedFiles(self):
		for zone in self.sz_controller.zones():
			self.sz_files += zone.fileList(True)

	def get_honeyDirectories(self):
		for zone in self.hz_controller.zones():
			self.hz_directories += zone.dirList()

	def generate_honeyFile(self, sz_file):
		name_extension = self.nameGenerator.generate(sz_file['fullpath'], sz_file['namingRules'])
		fullname = name_extension[0] + name_extension[1]
		path = self.pathGenerator.generate("", fullname)
		honeyFile_Fullpath = filesystem.fullpath(path, fullname)
		filesystem.clone(sz_file['fullpath'], honeyFile_Fullpath)
		request = {"function": "addHoneyFile", "data": {'protectedFile': sz_file['fullpath'], 'protectedFile_extension_stats': sz_file['object_ref'].extension_stats, 'honeyFile': honeyFile_Fullpath}}
		self.hz_controller.addRequest(request)
		self.sz_controller.addRequest(request)

	def check_backup(self, sz_file):
		protectedFile = sz_file['object_ref']

		if (len(protectedFile.honeyFiles) > 0):
			honeyFile_FullPath = sz_file['object_ref'].honeyFiles[-1]
			honeyFile = self.hz_controller.getHoneyFile(honeyFile_FullPath)
			if honeyFile :
				if protectedFile.isBackupNeeded(honeyFile['object_ref']):
					GUIManager.log("{} needs backup #1 - {}".format(sz_file['fullpath'], honeyFile_FullPath), end="\r")
					self.generate_honeyFile(sz_file)
			else:
				if filesystem.file_exists(honeyFile_FullPath, ""):
					honeyFile = self.hz_controller.addHoneyFile(protectedFile, sz_file['object_ref'].extension_stats, honeyFile_FullPath)
					GUIManager.log(honeyFile, end="\r")
					if protectedFile.isBackupNeeded(honeyFile):
						GUIManager.log("{} needs backup #1 - {}".format(sz_file['fullpath'], honeyFile_FullPath), end="\r")
						self.generate_honeyFile(sz_file)
				else:
					GUIManager.log("{} needs backup #2 - {}".format(sz_file['fullpath'], honeyFile_FullPath), end="\r")
					self.generate_honeyFile(sz_file)
		else:
			GUIManager.log("{} needs backup #3".format(sz_file['fullpath']), end="\r")
			self.generate_honeyFile(sz_file)

	def check_backups(self):
		GUIManager.log("-------------- Backup Checker -----------------")
		self.pathGenerator = PathGenerator(self.hz_directories)
		for sz_file in self.sz_files:
			self.check_backup(sz_file)

	def file_attacked(self):
		GUIManager.log("-------------- Attack Report -----------------")
		attacks = 0
		restores = 0
		for sz_file in self.sz_files:
			attacked = sz_file['object_ref'].wasAttacked()
			attacks += 1 if attacked else 0
			if attacked:
				GUIManager.log("{} was possibly attacked".format(sz_file['fullpath']))
				restored = self.sz_controller.restore(sz_file['object_ref'])
				restores += 1 if restored else 0
				if restored:
					GUIManager.log("{} was successfully restored".format(sz_file['fullpath']))

		GUIManager.log("Total of {} files were possibly attacked".format(attacks))
		GUIManager.log("Total of {} files were successfully restored".format(restores))
