'''
This file is part of FrontLine.

FrontLine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FrontLine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FrontLine.  If not, see <https://www.gnu.org/licenses/>.
'''

import persistent
import persistent.list
from pprint import pprint
import os, time, json, os.path, copy
from pathlib import Path
from pprint import pprint
from forbidden_zone.forbidden_zone import ForbiddenZone
from agents.win import filesystem

class ForbiddenZones(persistent.Persistent):
	path_zones = {}
	def __init__(self, driveMapper):
		cwd = filesystem.cwd()
		attributes = {
			"AllowedProcesses": [
				{'self': ['*']}
			]
		}

		self.path_zones[cwd] = attributes
		self.process_ForbiddenZones(driveMapper)

	def process_ForbiddenZones(self, driveMapper):
		self.ForbiddenZones = persistent.list.PersistentList()
		print(self.path_zones)
		for path in self.path_zones:
			path_sys = driveMapper.rmap(path)
			forbiddenZone = ForbiddenZone(path, path_sys, self.path_zones[path])
			self.add(forbiddenZone)

	def add(self, forbiddenZone):
		self.ForbiddenZones.append(forbiddenZone)
		self._p_changed = 1

	def zones(self):
		return self.ForbiddenZones

	def get(self, fullpath):
		for forbiddenZone in self.ForbiddenZones:
			if forbiddenZone.path() == fullpath:
				return True, forbiddenZone
		return False, None

	def getFileZone(self, file):
		length = 0
		zone = None
		filepath = filesystem.dirname(file).upper()
		for forbiddenZone in self.ForbiddenZones:
			if filepath.startswith(forbiddenZone.path.upper()):
				zone = forbiddenZone if len(forbiddenZone.path) > length else zone
		return zone

	def __str__(self):
		text = "////////// ForbiddenZones ////////////////" + "\n"
		for forbiddenZone in self.ForbiddenZones:
			text += str(forbiddenZone)
		return text
