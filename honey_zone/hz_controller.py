'''
This file is part of FrontLine.

FrontLine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FrontLine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FrontLine.  If not, see <https://www.gnu.org/licenses/>.
'''

import ZODB, ZODB.FileStorage, transaction
import persistent
import persistent.list

import queue, threading, time

from honey_zone.honey_zones import HoneyZones
from honey_zone.hz_anomaly_detector import HZ_AnomalyDetector
from utils.color import Color
from gui_manager import GUIManager

def threaded(fn):
	def wrapper(*args, **kwargs):
		thread = threading.Thread(target=fn, args=args, kwargs=kwargs)
		thread.start()
		return thread
	return wrapper

class HZ_Controller(threading.Thread):
	stop = False
	IDENTIFIER = "HZ_Controller"
	DB_ENTRY = 'honeyZones'
	DB_PATH = './storage/db/'
	SECURE_ZONE_FILE = '\\storage\\secure_zones.json'
	requestQueue = queue.Queue()
	eventQueue = queue.Queue()

	def __init__(self, conf_root, driveMapper, terminator):
		threading.Thread.__init__(self)
		self.intitialised = False
		self.anomalyDetector = HZ_AnomalyDetector(terminator)
		self.anomalyDetector.start()
		try:
			self.getDB()
			self.init_db(conf_root, driveMapper)
			GUIManager.log("£££££££££££££££££££££ HZ_Controller is intitialised £££££££££££££££££££££££££")
			self.intitialised = True
		except Exception as e:
				GUIManager.log("ERROR HZ __init__#0013: {}".format(e))

	def getDB(self):
		db_path = '{}/{}.fs'.format(self.DB_PATH, self.DB_ENTRY)
		storage = ZODB.FileStorage.FileStorage(db_path)
		c = threading.Condition()
		db = ZODB.DB(storage)
		self.transaction_manager = transaction.TransactionManager()
		connection = db.open(self.transaction_manager)
		self.root = connection.root()

	def init_db(self, conf_root, driveMapper):
		if self.exists():
			GUIManager.log("HoneyZones Were Found in the DB")
			self.honeyZones = self.root[self.DB_ENTRY]
		else:
			GUIManager.log("No HoneyZones Were Found in the DB")
			self.root[self.DB_ENTRY] = HoneyZones(conf_root, driveMapper)
			self.honeyZones = self.root[self.DB_ENTRY]
			self.transaction_manager.commit()
		self.fileList()
		self.fileListUpdater()
		self.eventReceiver()
		self.committer()


	def exists(self):
		return 	self.DB_ENTRY in self.root

	def zones(self):
		return self.honeyZones.zones()

	def dirList(self):
		return self.honeyZones.dirList()

	def fileList(self):
		self.honeyFileList = self.honeyZones.fileList()
		return self.honeyFileList

	def fileCount(self):
		return len(self.honeyFileList)

	def fileSize(self):
		return self.secureZones.fileSize()

	def addRequest(self, request):
		self.requestQueue.put(request)

	def isHoneyFile(self, file_fullpath):
		result = next((file for file in self.honeyFileList if file["fullpath"] == file_fullpath), None)
		return result

	def addNewEvent(self, event):
		self.eventQueue.put(event)

	def whatChanged(self, honeyFile):
		return self.honeyZones.whatChanged(honeyFile)

	def getHoneyFile(self, honeyFile_fullpath):
		for item in self.honeyFileList:
			if item["fullpath"] == honeyFile_fullpath:
				return item
		return None

	def addHoneyFile(self, protectedFile, protectedFile_extension_stats, honeyFile):
		honeyFileObject = self.honeyZones.addHoneyFile(protectedFile, protectedFile_extension_stats, honeyFile)
		self._p_changed = 1
		try:
			self.transaction_manager.commit()
		except Exception as e:
			GUIManager.log(Color.red("hz_controller:addHoneyFile |  Database is busy, changes might be lost"))
		return honeyFileObject

	def terminate(self):
		self.stop = True

	@threaded
	def eventReceiver(self):
		while not self.stop:
			while not self.eventQueue.empty() and not self.stop:
				try:
					event = self.eventQueue.get()
					if self.isHoneyFile(event.file_fullpath):
						self.anomalyDetector.submit(event)
					else:
						event.ignored(self.IDENTIFIER)
				except KeyboardInterrupt:
					exit()
				except Exception as e:
					GUIManager.log("ERROR HZ requestQueue_monitor#0011: {}".format(e))
			else:
				time.sleep(0.5)

	@threaded
	def committer(self):
		while not self.stop:
			try:
				self.transaction_manager.commit()
			except KeyboardInterrupt:
				exit()
			except Exception as e:
				GUIManager.log("ERROR HZ requestQueue_monitor#0015: {}".format(e))
			finally:
				time.sleep(2)
				pass

	@threaded
	def fileListUpdater(self):
		while not self.stop:
			try:
				self.fileList()
			except KeyboardInterrupt:
				exit()
			except Exception as e:
				GUIManager.log("ERROR HZ requestQueue_monitor#0012: {}".format(e))
			finally:
				time.sleep(10)
				pass

	def run(self):
		while not self.stop:
			try:
				while not self.requestQueue.empty() and not self.stop:
					request = self.requestQueue.get()
					self.dispatch(request)
			except KeyboardInterrupt:
				exit()
			except Exception as e:
				GUIManager.log("ERROR HZ requestQueue_monitor#002: {}".format(e))
			finally:
				time.sleep(1)
				pass

	def dispatch(self, request):
		function = request['function']
		data = request['data']
		try:
			if function == "addHoneyFile":
				self.honeyZones.addHoneyFile(data['protectedFile'], data['protectedFile_extension_stats'], data['honeyFile'])
			elif function == "updateProtectedFile":
				pass
			elif function == "newEventReceived":
				self.honeyZones.newEventReceived(data['protectedFile'], data['event'])
			else:
				pass
		except Exception as e:
			GUIManager.log(request)
			GUIManager.log("ERROR HZ requestQueue_monitor#001: {}".format(e))
