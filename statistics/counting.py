'''
This file is part of FrontLine.

FrontLine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FrontLine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FrontLine.  If not, see <https://www.gnu.org/licenses/>.
'''

import os, time, json, os.path
from pathlib import Path

def dir_stats(files, directories, items, depth):
	file_count = 0
	file_size  = 0
	extensions = {}
	subdirectory_count = 0
	maximum_depth = 0

	directory_stats = get_directory_stats(files, directories, items, depth)
	for directory_stat_key in directory_stats:
		extension_stat = directory_stats[directory_stat_key]['extensions']
		root_stats = directory_stats[directory_stat_key]

		for extension_key in extension_stat:
			extension = extension_stat[extension_key]
			if extension_key not in extensions:
				extensions[extension_key] = {
					'count': { 'total': extension['count']['total'], 'percentage': 0},
					'size':  { 'total': extension['size']['total'], 'percentage': 0}
				}
			else:
				extensions[extension_key]['count']['total'] += extension['count']['total']
			file_count += extension['count']['total']
			file_size  += extension['size']['total']
		subdirectory_count += root_stats['subdirectories']['count']
		maximum_depth = root_stats['subdirectories']['maximum_depth'] if root_stats['subdirectories']['maximum_depth'] > maximum_depth else maximum_depth

	for extension in extensions:
		extensions[extension]['count']['percentage'] = extensions[extension]['count']['total'] / file_count * 100
		extensions[extension]['size']['percentage'] = extensions[extension]['size']['total'] / file_size * 100

	stats = {
		'files': {
			'count': file_count,
			'size' : file_size
		},
		"subdirectories" : {
			'count': subdirectory_count, # len(directories)
			'depth': depth,
			'maximum_depth': maximum_depth + 1 if len(directories) > 0 else maximum_depth
		},
		'extensions': extensions
	}
	return stats

def get_directory_stats(files, directories, items, depth):
	directory_stats = {
		'.': {
			'subdirectories': {
				'count': len(directories),
				'depth': depth,
				'maximum_depth': 0
			},
			'extensions': get_extension_stat(files)
		}
	}

	for directory in directories:
		stats = items[directory.name]['stats']
		if (stats):
			directory_stats[directory.name] = stats

	return directory_stats

def get_extension_stat(files):
	extensions = {}
	file_count = len(files)
	file_size = 0
	for file in files:
		extension = Path(file.name).suffix
		if extension not in extensions:
			extensions[extension] = {
				'count': {'total': 0, 'percentage': 0},
				'size': { 'total': 0, 'percentage': 0}
			}
		extensions[extension]['count']['total'] += 1
		extensions[extension]['size']['total']  += file.stat().st_size
		file_size += file.stat().st_size

	for extension in extensions:
		extensions[extension]['count']['percentage'] = extensions[extension]['count']['total'] / file_count * 100
		extensions[extension]['size']['percentage'] = extensions[extension]['size']['percentage'] / file_size * 100

	return extensions
