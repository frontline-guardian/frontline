'''
This file is part of FrontLine.

FrontLine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FrontLine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FrontLine.  If not, see <https://www.gnu.org/licenses/>.
'''

import persistent
import persistent.list

class ProtectedFiles(persistent.Persistent):
	def __init__(self):
		self.protectedFiles = persistent.list.PersistentList()

	def add(self, protectedFile):
		self.protectedFiles.append(protectedFile)
		self._p_changed = 1

	def list(self):
		return self.protectedFiles

	def get(self, protectedFileName):
		for protectedFile in self.protectedFiles:
			if protectedFile.name.upper() == protectedFileName.upper():
				return protectedFile
		return None

	def __str__(self):
		text = ""
		for protectedFile in self.protectedFiles:
			text +=  str(protectedFile) + "\n"
		return text

	def fileList(self):
		list = []
		for protectedFile in self.protectedFiles:
			list += [{'fullpath': protectedFile.fullpath, 'object_ref': protectedFile}]

		return list

	def fileSize(self):
		size = 0
		for protectedFile in self.protectedFiles:
			size += protectedFile.fileSize()
		return size

	def addHoneyFile(self, protectedFileName, honeyFile):
		protectedFile = self.get(protectedFileName)
		if protectedFile:
			protectedFile.addHoneyFile(honeyFile)

	def newEventReceived(self, protectedFileName, event):
		protectedFile = self.get(protectedFileName)
		if protectedFile:
			protectedFile.newEventReceived(event)
