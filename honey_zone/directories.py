'''
This file is part of FrontLine.

FrontLine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FrontLine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FrontLine.  If not, see <https://www.gnu.org/licenses/>.
'''

import persistent
import persistent.list
from pprint import pprint

from agents.win import filesystem
from honey_zone.honey_files import HoneyFiles
from statistics import statistics

class Directories(persistent.list.PersistentList):
	def __init__(self,  path, settings, isRoot, recursive):
		self.isRoot = isRoot
		self.path = path
		self.settings = settings
		if path:
			self.init_honeyDirectories(recursive)
		#self.attributes = filesystem.attributes(file.stat())

	def init_honeyDirectories(self, recursive):
		self.honeyDirectories = persistent.list.PersistentList()
		if self.isRoot:
			self.addRoot(recursive)
		directories = filesystem.get_directories(self.path)
		for directory in directories:
			honeyDirectory = HoneyDirectory(directory, self.path, self.settings, recursive)
			self.honeyDirectories.append(honeyDirectory)
			self._p_changed = 1

	def addRoot(self, recursive):
		honeyDirectory = HoneyDirectory("", self.path, self.settings, recursive)
		self.honeyDirectories.append(honeyDirectory)
		self._p_changed = 1

	def listProtected(self):
		return self.honeyDirectories

	def get(self, name):
		for honeyDirectory in self.honeyDirectories:
			if honeyDirectory.name.upper() == name.upper():
				return honeyDirectory
		return None

	def __str__(self):
		text = "------------ Protected Directories: \n"
		for honeyDirectory in self.honeyDirectories:
			text += str(honeyDirectory)
		return text

	def dirList(self):
		list = []
		for directory in self.honeyDirectories:
			list += directory.dirList()
		return list

	def fileList(self):
		if (self.path == ""):
			print("Should be Empty")
			return []
		list = []
		for honeyDirectory in self.honeyDirectories:
			list += honeyDirectory.fileList()
		return list

	def fileSize(self):
		size = 0
		for honeyDirectory in self.honeyDirectories:
			size += honeyDirectory.fileSize()
		return size

	def addHoneyFile(self, protectedFile, protectedFile_extension_stats, honeyFile, subpathList):
		try:
			if len(subpathList) == 1:
				return self.get("").addHoneyFile(protectedFile, protectedFile_extension_stats, honeyFile, subpathList)
			elif len(subpathList) > 1:
				directory = self.get(subpathList[0])
				return directory.addHoneyFile(protectedFile, protectedFile_extension_stats, honeyFile, subpathList[1:])
			else:
				print("{}: honeyFile name is wrong @ Directories".format(honeyFile))
		except Exception as e:
			print("ERROR honey_directories addHoneyFile#0001: {}".format(e))
		finally:
			return None

	def whatChanged(self, honeyFile):
		if len(subpathList) == 1:
			self.get("").whatChanged(subpathList)
		elif len(subpathList) > 1:
			directory = self.get(subpathList[0])
			directory.whatChanged(subpathList[1:])
		else:
			pass

class HoneyDirectory(persistent.Persistent):
	def __init__(self,  directory, path, settings, recursive):
		self.path = path
		self.settings = settings
		if directory == "":
			self.name = ""
			self.fullpath = self.path
			self.hasSubdirectories = False
			self.directories = []
		else:
			self.name = directory.name
			self.fullpath = filesystem.fullpath(self.path, self.name)
			if recursive:
				self.directories = Directories(self.fullpath, self.settings, directory != ""  , recursive)
				self.hasSubdirectories = len(filesystem.get_directories(self.fullpath)) > 0
			else:
				self.hasSubdirectories = False
				self.directories = []

		print("Processing HoneyZone {}".format(self.fullpath))

		self.honeyFiles = HoneyFiles()

		self.attributes = None if self.name == "" else filesystem.attributes(directory.stat())
		self._p_changed = 1

	def __str__(self):
		text  = "----------------------------" + 'FullPath: {}\n'.format(self.fullpath)
		text += "----------------------------" + 'Path: {}\n'.format(self.path)
		text += "----------------------------" + 'Name: {}\n'.format(self.name)
		text += "----------------------------" + 'Attributes: {}\n'.format(self.attributes)
		text += "----------------------------{}\n".format(str(self.directories))

		return text

	def dirList(self):
		return [self.fullpath] + (self.directories.dirList() if self.hasSubdirectories else [])

	def fileList(self):
		list = []
		list += self.honeyFiles.fileList()
		if self.hasSubdirectories :
			list += self.directories.fileList()
		return list

	def fileSize(self):
		size = 0
		size += self.honeyFiles.fileSize()
		if self.hasSubdirectories :
			size += self.directories.fileSize()
		return size

	def __str__(self):
		text  = "----------------------------" + 'FullPath: {}\n'.format(self.fullpath)
		text += "----------------------------" + 'Path: {}\n'.format(self.path)
		text += "----------------------------" + 'Name: {}\n'.format(self.name)
		text += "----------------------------" + 'Attributes: {}\n'.format(self.attributes)
		text += "----------------------------{}\n".format(str(self.directories))
		text += "----------------------------:\n" + str(self.honeyFiles)
		#text += 'Parents: {}\n'.format(self.parents)

		return text

	def addHoneyFile(self, protectedFile, protectedFile_extension_stats, honeyFile, subpathList):
		try:
			if len(subpathList) == 1:
				return self.honeyFiles.addHoneyFile(protectedFile, protectedFile_extension_stats, honeyFile, subpathList[0])
			elif len(subpathList) > 1 and self.hasSubdirectories:
				return self.directories.addHoneyFile(protectedFile, protectedFile_extension_stats, honeyFile, subpathList)
			else:
				print("{}: honeyFile name is wrong @Directory {} - {} - {}".format(honeyFile, self.fullpath, subpathList, self.hasSubdirectories))
		except Exception as e:
			print("ERROR honey_directory addHoneyFile#0001: {}".format(e))
		finally:
			return None

	def whatChanged(self, honeyFile):
		if len(subpathList) == 1:
			self.honeyFiles.whatChanged(subpathList)
		elif len(subpathList) > 1 and self.hasSubdirectories:
			self.directories.whatChanged(subpathList)
		else:
			pass
