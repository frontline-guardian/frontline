'''
This file is part of FrontLine.

FrontLine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FrontLine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FrontLine.  If not, see <https://www.gnu.org/licenses/>.
'''

import os, time, json, os.path, secrets, datetime, random, string
from pathlib import Path
from pprint import pprint
from agents.win import filesystem

class NameGenerator:
	current_milli_time = lambda self: int(round(time.time() * 1000))

	def __init__(self):
		self.functions = ['aggregate', 'name', 'random_number', 'random_string', 'random_word', 'reverse_name', 'timestamp']

	def generate(self, filepath,  namingRules):
		name = filesystem.filename(filepath)
		self.file_extension = filesystem.extension(filepath)

		extension_index	= name.rfind(self.file_extension)
		self.file_name 	= name[:extension_index] if extension_index > 0 else name

		separator = namingRules['separators'][secrets.randbelow(len(namingRules['separators']))]
		extension = namingRules['extensions'][secrets.randbelow(len(namingRules['extensions']))]
		template  = namingRules['templates'][secrets.randbelow(len(namingRules['templates']))]

		template_parts = template.split("__")

		name_parts = []
		for template_part in template_parts:
			try:
				if (template_part in self.functions):
					name_parts.append(getattr(self, template_part)())
				else:
					name_parts.append(template_part)
			except Exception as e:
				name_parts.append(template_part)

		return self.aggregate(name_parts, separator), extension


	def timestamp(self):
		now = datetime.datetime.now()
		return now.strftime('%Y%m%d_%H%M%S%f')

	def random_string(self):
		letters = string.ascii_letters
		return ''.join(random.choice(letters) for i in range(stringLength))

	def reverse_name(self):
		return self.file_name[::-1]

	def random_word(self, separator, extension):
		#print('random_name__random_number')
		return "random"

	def random_number(self, max= 9999):
		return str(secrets.randbelow(max))

	def aggregate(self, name_parts, separator):
		name = ""
		for name_part in name_parts:
			if (name != ""):
				name = name + separator + name_part
			else:
				name = name_part
		#name += extension
		return name

	def name(self):
		return self.file_name

	def extension(self, extension):
		return (extension if extension != "." else self.file_extension)
